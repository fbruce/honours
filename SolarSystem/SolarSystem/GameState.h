#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <SDL.h>

class Game;

class GameState
{
public:
	virtual ~GameState() { return; }
	virtual void draw(SDL_Window *window) = 0;
	virtual void init(Game &context) = 0;
	virtual bool handleSDLEvent(Game &context, SDL_Event* sdlEvent) = 0;
	virtual void update() = 0;

};
#endif