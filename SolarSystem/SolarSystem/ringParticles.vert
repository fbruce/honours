#version 440
//430
uniform mat4 modelview;
uniform mat4 projection;

out vec4 ex_Color;

layout(location = 0) in vec3 positions;
layout(location = 1) in vec3 velocities;

float time;
float currentTime;

void main(void)
{
	vec3 newPosition = positions;
	vec3 newVelocity = velocities;
	//takes in postion - position currently saturn
	//takes in velocity
	vec3 newPosVec = newPosition;// += (newVelocity); // newPosition + newVelocity;
	vec4 vertexPosition = modelview * vec4(newPosVec,1.0);
	ex_Color = vec4(255.0f,250.0f,205.0f,1.0f);
	gl_Position = (projection * vertexPosition);
}
