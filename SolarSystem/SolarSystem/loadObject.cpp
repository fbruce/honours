#include "loadObject.h"

LoadObject::LoadObject(const char *filename)
{
	object = filename;
	rt3d::loadObj(object, verts, norms, tex_coords, indices);
	objectIndexCount = indices.size();
}
LoadObject::~LoadObject(void)
{

}
