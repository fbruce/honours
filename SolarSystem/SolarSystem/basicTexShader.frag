//texture shader
#version 440
//430
precision highp float;

uniform sampler2D textureUnit0;

in vec2 ex_TexCoord;

layout(location = 0) out vec4 out_Color;

void main(void)
{
	out_Color = texture(textureUnit0, ex_TexCoord);
}