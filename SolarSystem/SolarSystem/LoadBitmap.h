#ifndef LOADBITMAP_H
#define LOADBITMAP_H

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_main.h>
#include <iostream>

class LoadBitmap
{
private:

public:
	GLuint loadBitmap(char *fname);
};
#endif