#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "loadObject.h"
#include "LoadBitmap.h"
#include "GUI.h"
#include "Game.h"
#include <SDL.h>
#include <SDL_main.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <stack>
#include <vector>

int main(int argc, char *argv[]) 
{
	Game *newGame = new Game();
	newGame->init();
	newGame->run();

	return 0;
}