#include "Moon.h"

static glm::vec3 rotateVector = glm::vec3(0.0f, 1.0f, 0.0f);
Moon::Moon(Planet *parent, glm::vec3 StartPosition, GLfloat radius, GLuint mesh, GLuint meshIndex, GLfloat massOfPlanet, GLfloat orbitSpeed, GLfloat rotationSpeed, GLfloat distance)
{
	startPosition = parent->getStartPosition();
	m_position[0] = startPosition.x + 10.0f;
	m_position[1] = startPosition.y;
	m_position[2] = startPosition.z + 10.0f;
	m_radius = radius;
	m_scale = glm::vec3(radius, radius, radius);
	m_mesh = mesh;
	m_indexCount = meshIndex;
	m_mass = massOfPlanet;
	m_orbitSpeed = orbitSpeed;
	m_rotationSpeed = rotationSpeed;
	distanceFromParent = distance;

}
Moon::~Moon(void)
{
	delete this;
}
void Moon::init()
{

}
void Moon::draw(){};
void Moon::update(float time, Planet *parent)
{
	GLfloat dt =  time * 10;

	rotationAngle += m_rotationSpeed * dt;
	if (rotationAngle >= 360)
		rotationAngle = 0.0f;

	orbitAngle += m_orbitSpeed * dt;
	if (orbitAngle >= 360)
		orbitAngle = 0.0f;
}

glm::mat4 Moon::orbit(const GLuint shader, glm::mat4 modelview)
{
	modelview = glm::rotate(modelview, orbitAngle, rotateVector);
	return modelview;
}
glm::mat4 Moon::rotation(const GLuint shader, glm::mat4 modelview)
{
	modelview = glm::rotate(modelview, rotationAngle, rotateVector);
	return modelview;
}
void Moon::draw(const GLuint shader, glm::mat4 modelview, Planet *parentPlanet)
{
	modelview = translateOrbit(shader, modelview, parentPlanet);
	modelview = orbit(shader, modelview);
	modelview = rotation(shader, modelview);
	//modelview = glm::translate(modelview, glm::vec3(0.0f, 0.0f, 0.0f));
	//////this order carries out a rotation next to earth although it seems to change
	////modelview = translate(shader, modelview, parentPlanet);
	////modelview = orbit(shader, modelview);
	////modelview = translateOrbit(shader, modelview, parentPlanet);
	////modelview = rotation(shader, modelview);
	////modelview = scale(shader, modelview);


	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	rt3d::drawMesh(m_mesh, m_indexCount, GL_TRIANGLES);
}
glm::mat4 Moon::translate(const GLuint shader, glm::mat4 modelview, Planet *parentPlanet)
{
	GLfloat offset = 0.0f;
	m_position[0] = parentPlanet->getNewPosX();
	m_position[1] = 0.0f;
	m_position[2] = parentPlanet->getNewPosZ();
	modelview = glm::translate(modelview, glm::vec3(-m_position[0] - offset, -m_position[1], -m_position[2] - offset));
	return modelview;
}
glm::mat4 Moon::translateOrbit(const GLuint shader, glm::mat4 modelview, Planet *parentPlanet)
{
	//m_position[0] = parentPlanet->getNewPosX();
	//m_position[1] = 0.0f;
	//m_position[2] = parentPlanet->getNewPosZ();
	m_position[0] = parentPlanet->getNewScaledPosX();// / GlobalVariables::DrawingScale;// +(GlobalVariables::earthRadius * GlobalVariables::RadiusScale);
	m_position[1] = 0.0f;
	m_position[2] = parentPlanet->getNewScaledPosZ();// / GlobalVariables::DrawingScale;// +(GlobalVariables::earthRadius * GlobalVariables::RadiusScale);
	//move to planet position
	GLfloat offset = 0.0f;
	//cout << "X pos" << m_position[0] << endl;
	//cout << "Y pos" << m_position[1] << endl;
	//cout << "Z pos" << m_position[2] << endl;
	modelview = glm::translate(modelview, glm::vec3(-m_position[0], -m_position[1], -m_position[2]));
	//modelview = glm::translate(modelview, glm::vec3(newPosX, m_position[1], newPosZ));
	return modelview;
}
glm::mat4 Moon::scale(const GLuint shader, glm::mat4 modelview)
{
	GLfloat moonScale = m_scale.x / 10;
	modelview = glm::scale(modelview, glm::vec3(moonScale, moonScale, moonScale));
	return modelview;
}
float Moon::orbit()
{
	return 0.0f;
}
float Moon::rotation()
{
	return 0.0f;
}
