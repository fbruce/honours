#ifndef TIMER_H
#define TIMER_H

#include <time.h>

class Timer
{
public:
	float getCurrentTime();
	float convertToDays();
	float convertToYears();
	void init();
	float deltaTime();

private:
	time_t timer, startTime, timeElapsed, m_deltaTime;
};

#endif