#ifndef GUI_H
#define GUI_H

#include <iostream>
#include <SDL_ttf.h>
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "ShaderManager.h"
#include "rt3d.h"

class GUI
{
public:
	GUI();
	void textToTexture(const char *str);
	void draw(float x, float y, const GLuint shader);
	void exitFatalError(char *message);
	~GUI(void);

private:
	GLuint texID;
	GLuint height;
	GLuint width;
	GLuint mesh;
	//GLuint shaderID;
	//ShaderManager *shader;
	//GLuint guiShader;
};

#endif