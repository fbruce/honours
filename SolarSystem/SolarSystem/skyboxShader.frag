//skybox fragment shader
#version 440
//430
precision highp float;

out vec4 out_color;
in vec3 cubeTexCoord;
uniform samplerCube cubeMap;

void main(void)
{
	out_color = texture(cubeMap, cubeTexCoord);
}