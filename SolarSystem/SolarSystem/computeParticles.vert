#version 430

layout (location = 0) in vec4 position;

//out vec4 pos;
out vec4 ex_color;

layout (location = 0) uniform mat4 projection;
layout (location = 1) uniform mat4 modelview;

void main(void)
	{
		//pos = normalize(position);
		/*pos.z = 0.0;
		if(pos.x <= 0.0)
			pos.z += -pos.x;
		if(pos.y <= 0.0)
			pos.z += -pos.y;
		pos.z *= 0.5;*;*/

		vec4 particlePosition = modelview * position;
		ex_color = vec4(1.0f,1.0f,1.0f,1.0f);

		gl_Position = projection * particlePosition;
	}