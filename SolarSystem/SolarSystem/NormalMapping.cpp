#include "NormalMapping.h"

NormalMapping::~NormalMapping(void)
{
	delete this;
}
void NormalMapping::calculateTangents(vector<GLfloat> &tangents, vector<GLfloat> &verts, vector<GLfloat> &normals, vector<GLfloat> &tex_coords, vector<GLuint> &indices)
{
	// Code taken from http://www.terathon.com/code/tangent.html and modified slightly to use vectors instead of arrays
	// Lengyel, Eric. �Computing Tangent Space Basis Vectors for an Arbitrary Mesh�. Terathon Software 3D Graphics Library, 2001. 

	vector<glm::vec3> tan1(verts.size() / 3, glm::vec3(0.0f));
	vector<glm::vec3> tan2(verts.size() / 3, glm::vec3(0.0f));
	int triCount = indices.size() / 3;
	for (int c = 0; c < indices.size(); c += 3)
	{
		int i1 = indices[c];
		int i2 = indices[c + 1];
		int i3 = indices[c + 2];

		glm::vec3 v1(verts[i1 * 3], verts[i1 * 3 + 1], verts[i1 * 3 + 2]);
		glm::vec3 v2(verts[i2 * 3], verts[i2 * 3 + 1], verts[i2 * 3 + 2]);
		glm::vec3 v3(verts[i3 * 3], verts[i3 * 3 + 1], verts[i3 * 3 + 2]);

		glm::vec2 w1(tex_coords[i1 * 2], tex_coords[i1 * 2 + 1]);
		glm::vec2 w2(tex_coords[i2 * 2], tex_coords[i2 * 2 + 1]);
		glm::vec2 w3(tex_coords[i3 * 2], tex_coords[i3 * 2 + 1]);

		float x1 = v2.x - v1.x;
		float x2 = v3.x - v1.x;
		float y1 = v2.y - v1.y;
		float y2 = v3.y - v1.y;
		float z1 = v2.z - v1.z;
		float z2 = v3.z - v1.z;

		float s1 = w2.x - w1.x;
		float s2 = w3.x - w1.x;
		float t1 = w2.y - w1.y;
		float t2 = w3.y - w1.y;

		float r = 1.0F / (s1 * t2 - s2 * t1);
		glm::vec3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
			(t2 * z1 - t1 * z2) * r);
		glm::vec3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
			(s1 * z2 - s2 * z1) * r);

		tan1[i1] += sdir;
		tan1[i2] += sdir;
		tan1[i3] += sdir;

		tan2[i1] += tdir;
		tan2[i2] += tdir;
		tan2[i3] += tdir;
	}

	for (int a = 0; a < verts.size(); a += 3)
	{
		glm::vec3 n(normals[a], normals[a + 1], normals[a + 2]);
		glm::vec3 t = tan1[a / 3];

		glm::vec3 tangent;
		tangent = (t - n * glm::normalize(glm::dot(n, t)));

		// handedness
		GLfloat w = (glm::dot(glm::cross(n, t), tan2[a / 3]) < 0.0f) ? -1.0f : 1.0f;

		tangents.push_back(tangent.x);
		tangents.push_back(tangent.y);
		tangents.push_back(tangent.z);
		tangents.push_back(w);

	}
}
void NormalMapping::init()
{
	//how to access the shader created by the shaderManager
	//calculateTangents(tangents, object->getVertsData(), object->getNorms(), object->getTex(), object->getIndices());

	normalShader = shaders->getNormalShaderProgram();

	light0 = {
			{ 1.0f, 1.0f, 1.0f, 1.0f }, // ambient
			{ 1.0f, 1.0f, 1.0f, 1.0f }, // diffuse
			{ 1.0f, 1.0f, 1.0f, 1.0f }, // specular
			{ -10.0f, 10.0f, 10.0f, 1.0f }
	};

	material0 = { { 0.2f, 0.4f, 0.2f, 1.0f }, // ambient
	{ 0.5f, 1.0f, 0.5f, 1.0f }, // diffuse
	{ 0.0f, 0.1f, 0.0f, 1.0f }, // specular
	2.0f };//shiniess
	material1 = { { 0.4f, 0.4f, 1.0f, 1.0f }, // ambient
	{ 0.8f, 0.8f, 1.0f, 1.0f }, // diffuse
	{ 0.8f, 0.8f, 0.8f, 1.0f }, // specular
	1.0f };// shininess



	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, tangents.size()*sizeof(GLfloat) * 3, tangents.data(), GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)5, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(5);
	glBindVertexArray(0);

	//set uniforms for normal shader
	glUseProgram(*normalShader);
	rt3d::setLight(*normalShader, light0);
	rt3d::setMaterial(*normalShader, material1);
	GLuint uniformIndex = glGetUniformLocation(*normalShader, "attConst");
	uniformIndex = glGetUniformLocation(*normalShader, "attConst");
	glUniform1f(uniformIndex, attConstant);
	uniformIndex = glGetUniformLocation(*normalShader, "attLinear");
	glUniform1f(uniformIndex, attLinear);
	uniformIndex = glGetUniformLocation(*normalShader, "attQuadratic");
	glUniform1f(uniformIndex, attQuadratic);

	lightPos.x = -10.0f;
	lightPos.y = 10.0f;
	lightPos.z = 10.0f;
	lightPos.w = 1.0f;
}
void NormalMapping::draw()
{
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f, 800.0f / 600.0f, 1.0f, 5000.0f);

	glm::vec4 tmp = mvStack.top()*lightPos;
	light0.position[0] = 5.0f;
	light0.position[1] = 1.0f;
	light0.position[2] = 1.0f;
	rt3d::setLightPos(*normalShader, glm::value_ptr(tmp));//think I may need to use basic phong and tex shaders here

	glUseProgram(*normalShader);
	rt3d::setUniformMatrix4fv(*normalShader, "projection", glm::value_ptr(projection));
	rt3d::setLightPos(*normalShader, glm::value_ptr(tmp));

	//set texture, need to retrieve from create bitmap
	int textureLocation = glGetUniformLocation(*normalShader, "texture0");
	glUniform1i(textureLocation, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, normals->getTexture(0));

	//set normal
	int normalLocation = glGetUniformLocation(*normalShader, "normalTexture0");
	glUniform1i(normalLocation, 1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normals->getNormal(0));

	//set individual model matrix
	//glm::mat4 modelMatrix(1.0);
	//mvStack.push(mvStack.top());
	//modelMatrix = glm::translate(modelMatrix, glm::vec3(-2.0f, 1.0f, -3.0f));
	//modelMatrix = glm::rotate(modelMatrix, theta, glm::vec3(1.0f, 1.0f, 1.0f));
	//mvStack.top() = mvStack.top() * modelMatrix;
	//set uniform for camera positions
	//int uniformIndex = glGetUniformLocation(*normalShader, "cameraPos");
	//glUniform3fv(uniformIndex, 1, glm::value_ptr(camera->getEye()));
	//send uniforms to shader
	//rt3d::setUniformMatrix4fv(*normalShader, "modelMatrix", glm::value_ptr(modelMatrix));
	//rt3d::setUniformMatrix4fv(*normalShader, "modelview", glm::value_ptr(mvStack.top()));
	//draw mesh with normals
	//rt3d::drawIndexedMesh(meshObjects[0], meshIndexCount, GL_TRIANGLES);
	//mvStack.pop();



}