#include "ShaderManager.h"

ShaderManager::ShaderManager()
{
}

ShaderManager::~ShaderManager()
{

}
void ShaderManager::init()
{
	guiShaderProgram = initShaders("guiShader.vert", "guiShader.frag");
	guiShaderProgram = initShaders("guiShader.vert", "guiShader.frag");
	basicShaderProgram = initShaders("vertexShader.vert", "fragShader.frag");
	texturedShaderProgram = initShaders("basicTexShader.vert", "basicTexShader.frag");
	skyboxShaderProgram = initShaders("skyboxShader.vert", "skyboxShader.frag");
	normalShaderProgram = initShaders("normalMap.vert", "normalMap.frag");
	basicParticleProgram = initShaders("basicSphereParticles.vert", "basicSphereParticles.frag");
	ringParticleProgram = initShaders("ringParticles.vert", "ringParticles.frag");
	advancedParticleProgram = initShaders("flocking.vert", "flocking.frag");
	computeParticleShader = initShaders("computeParticles.vert", "computeParticles.frag");
	computeShader = initComputeShader("computeParticles.comp");
	haloShader = initShaders("haloShader.vert", "haloShader.frag");
}
char* ShaderManager::loadFile(const char *fname, GLint &fSize)
{
	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	ifstream file(fname, ios::in | ios::binary | ios::ate);
	if (file.is_open()) {
		size = (int)file.tellg(); // get location of file pointer i.e. file size
		fSize = (GLint)size;
		memblock = new char[size];
		file.seekg(0, ios::beg);
		file.read(memblock, size);
		file.close();
		cout << "file " << fname << " loaded" << endl;
	}
	else {
		cout << "Unable to open file " << fname << endl;
		fSize = 0;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
		return nullptr;
	}
	return memblock;
}
GLuint ShaderManager::initShaders(const char *vertFile, const char *fragFile)
{
	GLuint p, f, v;

	char *vs = 0; char *fs = 0;

	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);

	// load shaders & get length of each
	GLint vlen;
	GLint flen;
	vs = loadFile(vertFile, vlen);
	fs = loadFile(fragFile, flen);

	const char * vv = vs;
	const char * ff = fs;

	glShaderSource(v, 1, &vv, &vlen);
	glShaderSource(f, 1, &ff, &flen);

	GLint compiled;

	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		cout << "Vertex shader not compiled." << endl;
			printShaderError(v);
	}

	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		cout << "Fragment shader not compiled." << endl;
			printShaderError(f);
	}

	p = glCreateProgram();

	glAttachShader(p, v);
	glAttachShader(p, f);

	glBindAttribLocation(p, RT3D_VERTEX, "in_Position");
	glBindAttribLocation(p, RT3D_COLOUR, "in_Color");
	glBindAttribLocation(p, RT3D_NORMAL, "in_Normal");
	glBindAttribLocation(p, RT3D_TEXCOORD, "in_TexCoord");

	glLinkProgram(p);
	glUseProgram(p);

	delete[] vs; // dont forget to free allocated memory
	delete[] fs; // we allocated this in the loadFile function...

	return p;
}

void ShaderManager::printShaderError(const GLint shader)
{
	int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(shader))
		glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(shader))
			glGetProgramInfoLog(shader, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(shader, maxLength, &logLength, logMessage);
		cout << "Shader Info Log:" << endl << logMessage << endl;
		delete[] logMessage;
	}
}

void ShaderManager::exitFatalError(const char *message)
{
	cout << message << " ";
	exit(1);
}

GLuint ShaderManager::initComputeShader(const char *computeFile)
{
	GLuint compShader, program;
	compShader = glCreateShader(GL_COMPUTE_SHADER);

	if (0 == compShader)
	{
		fprintf(stderr, "Error creating comp shader.\n");
		exit(1);
	}

	const GLchar *compCode;
	GLint cLen;
	compCode = loadFile(computeFile, cLen);
	glShaderSource(compShader, 1, &compCode, &cLen);

	glCompileShader(compShader);
	GLint result;
	glGetShaderiv(compShader, GL_COMPILE_STATUS, &result);
	if (GL_FALSE == result)
	{
		fprintf(stderr, "Frag shader compilation failed.\n");
		GLint logLen;
		glGetShaderiv(compShader, GL_INFO_LOG_LENGTH, &logLen);
		if (logLen > 0)
		{
			char *log = (char *)malloc(logLen);
			GLsizei written;
			glGetShaderInfoLog(compShader, logLen, &written, log);
			fprintf(stderr, "Shader log:\n%s", log);
			free(log);
		}
	}

	program = glCreateProgram();
	if (0 == program)
	{
		fprintf(stderr, "Error creating program object.\n");
		exit(1);
	}

	glAttachShader(program, compShader);
		
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &result);
	if (GL_FALSE == result)
	{
		fprintf(stderr, "Failed to link shader program!\n");
		GLint logLen;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);
		if (logLen > 0)
		{
			char *log = (char *)malloc(logLen);
			GLsizei written;
			glGetShaderInfoLog(program, logLen, &written, log);
			fprintf(stderr, "Program log:\n%s", log);
			free(log);
		}
		else
			glUseProgram(program);
	}
	delete[] compCode;

	return program;
}


