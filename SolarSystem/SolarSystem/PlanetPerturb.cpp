#include "PlanetPerturb.h"

PlanetPerturb::PlanetPerturb(GLfloat mass, GLfloat radius, glm::vec3 position, GLfloat rotationSpeed, GLfloat orbitSpeed, GLuint mesh, GLuint meshIndex)
{
	this->mass = mass;
	this->radius = radius;
	this->position = position;
	this->rotationSpeed = rotationSpeed;
	this->orbitSpeed = orbitSpeed;
	this->mesh = mesh;
	this->meshIndex = meshIndex;
}
GLfloat PlanetPerturb::orbit()
{
	//glm::vec3 &position = this->position;
	//GLfloat &orbitSpeed = this->orbit;
	return 0.0f;
}
GLfloat PlanetPerturb::perturbation(PlanetPerturb *A, PlanetPerturb *B)
{
	//calculate overall gravitational force, when above a certain amount change orbit (return a value to effect orbit)
	//F = GConst * m1 * m2 / rSquared
	GLfloat gravForce = 0.0;
	GLfloat perturbAmount = 0.0f; //usually zero
	GLfloat mass1 = A->getMass();
	GLfloat mass2 = B->getMass();
	glm::vec3 pos1 = A->getPosition();
	glm::vec3 pos2 = B->getPosition();
	//calculate distance between two points
	GLfloat distance = 0.0f;
	distance = sqrt((pos1.x + pos2.x)- (pos1.z + pos2.z));
	gravForce = (gravConstant * mass1 * mass2)/distance;
	cout << gravForce << endl;
	//cout << distance << endl;
	GLfloat distanceBetweenBodies;
	return perturbAmount;
}
PlanetPerturb::~PlanetPerturb(void)
{

}
void PlanetPerturb::init()
{

}
void PlanetPerturb::draw(){};

void PlanetPerturb::draw(const GLuint shader, glm::mat4 modelview)
{
	GLfloat orbitalSpeed;
	glm::vec3 position;
	const glm::vec3 rotateVector = glm::vec3(0.0f, 1.0f, 0.0f);
	orbitalSpeed = this->orbitSpeed;
	position = this->position;

	modelview = glm::rotate(modelview, orbitalSpeed, rotateVector);
	modelview = glm::translate(modelview, glm::vec3(position.x, position.y, position.z));
	modelview = glm::scale(modelview, glm::vec3(1.0f,1.0f,1.0f));

	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	rt3d::drawMesh(mesh, meshIndex, GL_TRIANGLES);
}

float PlanetPerturb::rotation()
{
	return 0.0f;
}