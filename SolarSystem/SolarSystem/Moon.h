#ifndef MOON_H
#define MOON_H

#include<GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <iostream>
#include <stack>
#include "rt3d.h"
#include "ShaderManager.h"
#include "Body.h"
#include "Planet.h"

using namespace std;

class Moon : public Body
{
private:
	GLfloat m_mass;
	GLfloat m_radius;
	//GLfloat x, y, z;
	GLuint m_indexCount = 0;
	GLuint m_mesh;//mesh for drawing with
	GLfloat m_orbitSpeed;
	GLfloat m_rotationSpeed;
	glm::vec3 m_scale;
	GLuint texture;
	GLfloat planetRotation = 0.0f;
	glm::vec3 startPosition;
	GLuint shaderID;
	stack <glm::mat4> mvStack;//modelview stack
	GLfloat m_position[3];
	GLfloat rotationAngle = 0;
	GLfloat orbitAngle = 0;
	Planet *parentPlanet;
	GLfloat distanceFromParent;

	//start positions of planets, may read in from file


public:
	//Planet(GLfloat radius, GLuint texture, glm::vec3 startPosition);
	Moon(Planet *parent, glm::vec3 startPosition, GLfloat radius, GLuint mesh, GLuint meshIndex, GLfloat mass, GLfloat orbitSpeed, GLfloat rotationSpeed, GLfloat distance);
	~Moon();
	void init();
	void draw();
	void update(float time, Planet *parentPlanet);
	void draw(const GLuint shader, glm::mat4 modelview, Planet *parentPlanet);
	float rotation();//think needs to be a float
	float orbit();
	glm::mat4 orbit(const GLuint shader, glm::mat4 modelview);
	glm::mat4 rotation(const GLuint shader, glm::mat4 modelview);
	glm::mat4 translate(const GLuint shader, glm::mat4 modelview, Planet *parentPlanet);
	glm::mat4 scale(const GLuint shader, glm::mat4 modelview);
	glm::mat4 translateOrbit(const GLuint shader, glm::mat4 modelview, Planet *parentPlanet);
};
#endif