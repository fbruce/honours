#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <stack>
#include <vector>
#include "rt3d.h"

using namespace std;

class ShaderManager
{
private:
	GLuint basicShaderProgram;
	GLuint texturedShaderProgram;
	GLuint skyboxShaderProgram;
	GLuint normalShaderProgram;
	GLuint guiShaderProgram;
	GLuint basicParticleProgram;
	GLuint ringParticleProgram;
	GLuint advancedParticleProgram;
	GLuint computeShader;
	GLuint computeParticleShader;
	GLuint haloShader;

public:
	ShaderManager();
	~ShaderManager();
	char* loadFile(const char *fname, GLint &fSize);
	GLuint initShaders(const char *vertFile, const char *fragFile);
	void printShaderError(const GLint shader);
	void exitFatalError(const char *message);
	void init();
	GLuint initComputeShader(const char *computeFile);
	GLuint* getBasicShaderProgram(){ return &basicShaderProgram; }
	GLuint* getTexturedShaderProgram(){ return &texturedShaderProgram; }
	GLuint* getSkyboxShaderProgram(){ return &skyboxShaderProgram; }
	GLuint* getNormalShaderProgram(){ return &normalShaderProgram; }
	GLuint* getGuiShaderProgram(){ return &guiShaderProgram; }
	GLuint* getBasicParticleProgram(){ return &basicParticleProgram; }
	GLuint* getRingParticleProgram(){ return &ringParticleProgram; }
	GLuint* getFlockingProgram(){ return &advancedParticleProgram; }
	GLuint* getComputeShader(){ return &computeShader; }
	GLuint* getComputeParticlesProgram(){ return &computeParticleShader; }
	GLuint *getHaloShaderProgram(){ return &haloShader; }


};

#endif