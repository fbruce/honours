#ifndef PLANETPERTURB_H
#define PLANETPERTURB_H


//#include<GL/glew.h>
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>
//#include "Body.h"
//#include <vector>
//#include <iostream>
//#include <stack>
//#include "rt3d.h"
//
//using namespace std;
//
//class PlanetPerturb : public Body
//{
//private:
//	GLfloat const gravConstant = 0.000000000067;
//	GLfloat mass; //used to calculate gravitational force
//	GLfloat radius;//used to calculate gravitational force
//	glm::vec3 position;//used to calculate and modify position for gravitational force
//	GLfloat rotationSpeed;
//	GLfloat orbitSpeed;
//	GLuint mesh;
//	GLuint meshIndex = 0;
//	//const glm::vec3 rotateVector = glm::vec3(0.0f, 1.0f, 0.0f);
//
//
//
//public:
//	PlanetPerturb(GLfloat mass, GLfloat radius, glm::vec3 position, GLfloat rotationSpeed, GLfloat orbitSpeed, GLuint mesh, GLuint meshIndex);
//	~PlanetPerturb();
//	GLfloat rotation();
//	GLfloat orbit();
//	GLfloat perturbation(PlanetPerturb *A, PlanetPerturb *B);
//	GLfloat getMass(){ return mass; }
//	GLfloat getRadius(){ return radius; }
//	glm::vec3 getPosition(){ return position; }
//	GLfloat getRotationSpeed(){ return rotationSpeed; }
//	GLfloat getOrbitSpeed(){ return orbitSpeed; }
//	void draw();
//	void init();
//	void draw(const GLuint shader, glm::mat4 modelview);
//	
//};

#endif