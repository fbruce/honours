#include "Planet.h"

glm::vec3 rotateVector = glm::vec3(0.0f, 1.0f, 0.0f);
glm::vec4 blankVector(1.0, 1.0, 1.0, 1.0);
Planet::Planet(GLint ID, glm::vec3 startPosition, GLfloat radius, GLfloat scale, GLuint mesh, GLuint meshIndex, GLfloat massOfPlanet, GLfloat orbitSpeed, GLfloat rotationSpeed)
{
	//sets up a planet object
	m_position[0] = startPosition.x;
	m_position[1] = startPosition.y;
	m_position[2] = startPosition.z;
	m_ID = ID;
	m_radius = radius;
	m_scale = glm::vec3(scale, scale, scale);
	m_mesh = mesh;
	m_indexCount = meshIndex;
	m_mass = massOfPlanet;
	m_orbitSpeed = orbitSpeed;
	m_rotationSpeed = rotationSpeed;
	perturbed = false;
}
Planet::~Planet(void)
{
	
}
void Planet::init(){}
void Planet::draw(){};
void Planet::update(float time)
{
	GLfloat dt = time * 10;//real time, if speeding up to one tick a day * 365

	//takes rotation speed from planet, adds to angle at rate of time, when angle reaches 360 clears and starts again
	rotationAngle += m_rotationSpeed * dt;
	if (rotationAngle >= 360)
		rotationAngle = 0.0f;

	//As above with orbital speed
	orbitAngle += m_orbitSpeed * dt;
	if (orbitAngle >= 360)
		orbitAngle = 0.0f;
}

glm::mat4 Planet::orbit(const GLuint shader, glm::mat4 modelview)
{
	//orbits planet based on orbit angle and standard rotate vector to tell openGL which axis to rotate on
	modelview = glm::rotate(modelview, orbitAngle, rotateVector);
	return modelview;
}
glm::mat4 Planet::rotation(const GLuint shader, glm::mat4 modelview)
{
	//rotates planet using angle and Y vector
	modelview = glm::rotate(modelview, rotationAngle, rotateVector);
	return modelview;
}
void Planet::draw(const GLuint shader, glm::mat4 modelview, glm::vec3 viewMatrix)//sends shader and specific modelview, along with coordinates of camera
{
	//translations, Orbit must be carried out first to be popped off matrix last
	modelview = orbit(shader, modelview);
	modelview = translate(shader, modelview);
	modelview = scale(shader, modelview);
	modelview = rotation(shader, modelview);

	//modelview = moon->orbit(shader, modelview);
	//modelview = moon->rotation(shader, modelview);
	//modelview = moon->translateOrbit(shader, planet);

	//retrieves camera coordinates so they can be removed from modelview to retrieve specific object coordinates
	glm::mat4 view = glm::mat4(1.0f);
	view[3][0] = viewMatrix.x;
	view[3][1] = viewMatrix.y;
	view[3][2] = viewMatrix.z;//view holds camera position
	//view = glm::inverse(view);//inverses


	//creates new positions matrix from copy of modelview
	newPositions = modelview;

	// retrieves model position data from the last column of matrix and removes camera position (and scale)
	savedNewPositions.x = newPositions[3][0] - view[3][0]- m_scale[0];
	savedNewPositions.z = newPositions[3][2] - view[3][2] - m_scale[2];

	//sets matrix and draws mesh
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	rt3d::drawMesh(m_mesh, m_indexCount, GL_TRIANGLES);	
}
glm::mat4 Planet::translate(const GLuint shader, glm::mat4 modelview)
{
	if (perturbed == true)
	{
		modelview = glm::translate(modelview, glm::vec3(m_position[0] + 10.0f, m_position[1], m_position[2] + 10.0f));
		perturbed = false;
		//cout << "HIT" << endl;
	}
	else
		modelview = glm::translate(modelview, glm::vec3(m_position[0], m_position[1], m_position[2]));
	return modelview;
}
glm::mat4 Planet::scale(const GLuint shader, glm::mat4 modelview)
{
	//scales planet
	modelview = glm::scale(modelview, m_scale);
	return modelview;
}
float Planet::orbit()
{
	return 0.0f;
}
float Planet::rotation()
{
	return 0.0f;
}
void Planet::perturb(Planet *planet, Planet *otherPlanet, GLint ID)
{
	GLfloat newDistance = calculateDistance(planet, otherPlanet);
	//cout << "current distance" << newDistance << endl;
	//cout << "minimum distance" << GlobalVariables::mercuryVenusMinDistance.x;
	//newDistance = convertKMtoAU(newDistance);
	//cout << newDistance << endl;
	switch (ID)
	{
	case 1:
		if (newDistance < GlobalVariables::mercuryVenusMinDistance)
			//cout << "HIT" << endl;
			planet->perturbed = true;
		break;
	case 2:
		if (newDistance < GlobalVariables::venusEarthMinDistance)
			planet->perturbed = true;
		break;
	default:
		planet->perturbed = false;
		break;
	}

}
GLfloat Planet::gravitationalForce(Planet *planet, Planet *otherPlanet)
{
	//gets mass and radius of planets whose force is to be calculated
	GLfloat massA = planet->getMass();
	GLfloat massB = otherPlanet->getMass();
	GLfloat radiusA = planet->getRadius();
	GLfloat radiusB = otherPlanet->getRadius();
	GLfloat distance;
	//calculates distance between planets
	distance = calculateDistance(planet, otherPlanet);
	distance = convertAUtoKM(distance);
	GLfloat force;
	if (distance > 0)
	{
		force = GlobalVariables::GravitationalConstant * massA * massB / distance;
		//cout << force << endl;
	}
	else 
		force = 0.0f;

	return force;

}
GLfloat Planet::calculateDistance(Planet *planet, Planet *otherPlanet)
{
	//gets X and Z position of planets, Y is discarded as always zero
	//dividng by the global variable drawing scale removes it from the original AU measurement
	GLfloat distance=0;
	GLfloat currentXA = planet->getNewPosX() / GlobalVariables::DrawingScale;
	GLfloat currentXB = otherPlanet->getNewPosX() / GlobalVariables::DrawingScale;
	GLfloat currentZA = planet->getNewPosZ() / GlobalVariables::DrawingScale;
	GLfloat currentZB = otherPlanet->getNewPosZ() / GlobalVariables::DrawingScale;
	//calculates distance between X and Z coordinates
	GLfloat distX = currentXB - currentXA;//in AU
	GLfloat distZ = currentZB - currentZA;//in AU
	//Pythagoras to calculate distance
	distance = sqrt(distX * distX) + (distZ * distZ);//in AU
	//cout << distance << endl;
	//Multiplies by Scaler to remove from equation
	//distance = distance * GlobalVariables::AstronomicalUnitScale;
	//Converts from AU to KM to use in gravitational force equation
	//distance = convertAUtoKM(distance);
	return distance;//returns distance in AU
}
GLfloat Planet::convertAUtoKM(GLfloat AU)
{
	//convers AU position to KM for gravitational force
	KMPosition = AU * 149597871;
	return KMPosition;
}
GLfloat Planet::convertKMtoAU(GLfloat KM)
{
	GLfloat AUPosition;
	AUPosition = (KM/ GlobalVariables::AstronomicalUnitScale);//;
	AUPosition = AUPosition/ 149597871;
	return AUPosition;
}
GLfloat Planet::calculateMinDistance(Planet *planet, Planet *otherPlanet)
{
	GLfloat minDistance = 0;
	return minDistance;
}
