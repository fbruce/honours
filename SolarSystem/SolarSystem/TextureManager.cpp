#include "TextureManager.h"

TextureManager::TextureManager()
{
	loadBitmap = new LoadBitmap();
}
void TextureManager::init()
{
	textures[0] = loadBitmap->loadBitmap("PlanetTextures/earthTexture.bmp");
	textures[1] = loadBitmap->loadBitmap("PlanetTextures/plutoTexture.bmp");
	textures[2] = loadBitmap->loadBitmap("PlanetTextures/sunTexture.bmp");
	textures[3] = loadBitmap->loadBitmap("PlanetTextures/mercuryTexture.bmp");
	textures[4] = loadBitmap->loadBitmap("PlanetTextures/venusTexture.bmp");
	textures[5] = loadBitmap->loadBitmap("PlanetTextures/marsTexture.bmp");
	textures[6] = loadBitmap->loadBitmap("PlanetTextures/jupiterTexture.bmp");
	textures[7] = loadBitmap->loadBitmap("PlanetTextures/saturnTexture.bmp");
	textures[8] = loadBitmap->loadBitmap("PlanetTextures/uranusTexture.bmp");
	textures[9] = loadBitmap->loadBitmap("PlanetTextures/neptuneTexture.bmp");

	normals[0] = loadBitmap->loadBitmap("NormalMaps/earthNormal.bmp");
	normals[1] = loadBitmap->loadBitmap("NormalMaps/plutoNormal.bmp");
	normals[2] = loadBitmap->loadBitmap("NormalMaps/sunNormal.bmp");
	normals[3] = loadBitmap->loadBitmap("NormalMaps/mercuryNormal.bmp");
	normals[4] = loadBitmap->loadBitmap("NormalMaps/venusNormal.bmp");
	normals[5] = loadBitmap->loadBitmap("NormalMaps/marsNormal.bmp");
	normals[6] = loadBitmap->loadBitmap("NormalMaps/jupiterNormal.bmp");
	normals[7] = loadBitmap->loadBitmap("NormalMaps/saturnNormal.bmp");
	normals[8] = loadBitmap->loadBitmap("NormalMaps/uranusNormal.bmp");
	normals[9] = loadBitmap->loadBitmap("NormalMaps/neptuneNormal.bmp");

}
GLuint TextureManager::getTexture(int tex)
{
	GLuint texture[11];
	for (int i = 0; i < tex; i++)
	{
		texture[tex] = textures[i];
	}
	return tex;
}
GLuint TextureManager::getNormal(int tex)
{
	GLuint normal[11];
	for (int i = 0; i < tex; i++)
	{
		normal[tex] = normals[i];
	}
	return tex;
}
