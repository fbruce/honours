#ifndef PARTICLE_H
#define PARTICLE_H

#define _USE_MATH_DEFINES

#include "Planet.h"
#include <math.h>

class Particle
{
public:
	virtual ~Particle() { return; }
	virtual void init() = 0;
	virtual void update(Planet *planet) = 0;
	virtual void draw() = 0;
};
#endif