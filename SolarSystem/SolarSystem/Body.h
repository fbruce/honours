#ifndef BODY_H
#define BODY_H

class Body
{
public:
	virtual ~Body() {return;}
	virtual float rotation() = 0;
	virtual float orbit() = 0;
	virtual void init() = 0;
	virtual void draw() = 0;
};
#endif