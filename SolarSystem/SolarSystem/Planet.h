#ifndef PLANET_H
#define PLANET_H

//need to work out measurements, currently km/s
#define SPEED_OF_LIGHT 299792.458
//check the number of zeros is right
#define GRAVITATIONAL_CONSTANT 0000000066734

#include<GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <iostream>
#include <stack>
#include "rt3d.h"
#include "ShaderManager.h"
#include "Body.h"
#include "GlobalVariables.h"
//#include "Moon.h"

using namespace std;
using namespace GlobalVariables;

class Planet : public Body
{
private:
	GLfloat m_mass;
	GLfloat m_radius;
	GLuint m_indexCount = 0;
	GLuint m_mesh;//mesh for drawing with
	GLfloat m_orbitSpeed;
	GLfloat m_rotationSpeed;
	GLint m_ID;
	glm::vec3 m_scale;
	glm::vec4 savedNewPositions;
	GLuint texture;
	GLfloat planetRotation = 0.0f;
	glm::vec3 startPosition;
	GLuint shaderID;
	stack <glm::mat4> mvStack;//modelview stack
	GLfloat m_position[3];
	GLfloat rotationAngle = 0;
	GLfloat orbitAngle = 0;
	Planet *NeighbourA, *NeighbourB, *parentPlanet;
	GLfloat newPositionX;
	GLfloat newPositionZ;
	GLfloat KMPosition;
	GLfloat distanceFromSun;
	bool perturbed;
	glm::mat4 newPositions;
	//Moon *moon;
public:
	Planet(GLint ID, glm::vec3 startPosition, GLfloat radius, GLfloat scale, GLuint mesh, GLuint meshIndex, GLfloat mass, GLfloat orbitSpeed, GLfloat rotationSpeed);
	~Planet();
	void init();
	void draw();
	void update(float time);
	void draw(const GLuint shader, glm::mat4 modelview, glm::vec3 viewMatrix);
	float rotation();//think needs to be a float
	float orbit();
	glm::mat4 orbit(const GLuint shader, glm::mat4 modelview);
	glm::mat4 rotation(const GLuint shader, glm::mat4 modelview);
	glm::mat4 translate(const GLuint shader, glm::mat4 modelview);
	glm::mat4 scale(const GLuint shader, glm::mat4 modelview);
	void perturb(Planet *planet, Planet *otherPlanet, GLint ID);
	GLfloat gravitationalForce(Planet *planet, Planet * otherPlanet);
	GLfloat getOrbitSpeed(){ return m_orbitSpeed; }
	GLfloat getRotationSpeed(){ return m_rotationSpeed; }
	Planet getPlanet(){ return *parentPlanet; }
	GLfloat getMass(){ return m_mass; }
	GLfloat getRadius(){ return m_radius; }
	GLfloat calculateDistance(Planet *planet, Planet *otherPlanet);
	GLfloat getNewPosX(){ return savedNewPositions.x; }
	GLfloat getNewPosZ(){ return savedNewPositions.z; }
	GLfloat convertAUtoKM(GLfloat AU);
	GLfloat convertKMtoAU(GLfloat KM);
	GLfloat calculateMinDistance(Planet *planet, Planet *otherPlanet);
	GLint getID(){ return m_ID; }
	GLfloat getScale(){ return m_scale[0]; }
	glm::vec3 getStartPosition(){return glm::vec3(m_position[0], m_position[1], m_position[2]); }
	GLfloat getNewScaledPosX(){ return newPositions[3][0]; }
	GLfloat getNewScaledPosZ(){ return newPositions[3][2]; }
};
#endif