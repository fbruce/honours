#version 430

uniform mat4 modelview;
uniform mat4 projection;

in  vec3 in_Position;
in  vec3 in_Color;

out vec4 ex_Color;

void main(void)
{
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	ex_Color = vec4(in_Color,1.0);
	gl_Position = (projection * vertexPosition);
}