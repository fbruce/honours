#include "ComputeShaderParticles.h"

ComputeShaderParticles::ComputeShaderParticles(const int n)
{
	this->numOfParticles = n;
	if (numOfParticles <= 0)
		return;

	positions = new glm::vec4[numOfParticles];
	velocities = new glm::vec4[numOfParticles];


	for (int i = 0; i < numOfParticles; i++)
	{

		positions[i][0] =(float(std::rand()) / RAND_MAX) * 1.0 - 0.5;
		//std::cout << positions[i][0] << std::endl;
		positions[i][1] = (float(std::rand()) / RAND_MAX) * 1.0 - 0.5;
		//std::cout << positions[i][1] << std::endl;
		positions[i][2] = (float(std::rand()) / RAND_MAX) * 1.0 - 3.5;
		//std::cout << positions[i][2] << std::endl;
		positions[i][3] = 1.0f;

		for (int j = 0; j < 4; j++)
		{
			velocities[i][j] = 0.0f;
		}
	}

	GLuint bufferSize = numOfParticles * sizeof(glm::vec4);
	GLuint positionBuffer;
	glGenBuffers(1, &positionBuffer);
	//glBindVertexArray(vao[0]);
	//sets up data for going into compute shader
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, positionBuffer);
	glBufferData(GL_SHADER_STORAGE_BUFFER, bufferSize, positions, GL_DYNAMIC_DRAW);

	GLuint velocityBuffer;
	glGenBuffers(1, &velocityBuffer);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, velocityBuffer);
	glBufferData(GL_SHADER_STORAGE_BUFFER, bufferSize, velocities, GL_DYNAMIC_DRAW);

	glGenVertexArrays(1, vao);
	glBindVertexArray(*vao);
	//sets up data for frag shader
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	//glBindVertexArray(0);
	//GLuint bufferSize = numOfParticles * sizeof(glm::vec4);

	//glGenBuffers(2, vbo);
	//glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, vbo[0]);
	//glBufferData(GL_SHADER_STORAGE_BUFFER, bufferSize, positions, GL_DYNAMIC_DRAW);

	//glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, vbo[1]);
	//glBufferData(GL_SHADER_STORAGE_BUFFER, bufferSize, velocities, GL_DYNAMIC_DRAW);

	//glGenVertexArrays(1, vao);
	//glBindVertexArray(*vao);

	//glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	//glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	//glEnableVertexAttribArray(0);
	//glBindVertexArray(0);
}
void ComputeShaderParticles::init()
{
	//GLuint bufferSize = numOfParticles * sizeof(glm::vec4);

	//glGenBuffers(2, vbo);
	////glBindVertexArray(vao[0]);

	//glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, vbo[0]);
	//glBufferData(GL_SHADER_STORAGE_BUFFER, bufferSize, positions, GL_DYNAMIC_DRAW);
	////glEnableVertexAttribArray(0);

	//glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, vbo[1]);
	//glBufferData(GL_SHADER_STORAGE_BUFFER, bufferSize, velocities, GL_DYNAMIC_DRAW);
	////glEnableVertexAttribArray(1);

	//glGenVertexArrays(1, vao);
	//glBindVertexArray(vao[0]);

	//glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	//glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	//glEnableVertexAttribArray(0);

	//glBindVertexArray(0);

}
ComputeShaderParticles::~ComputeShaderParticles(void)
{
	delete positions;
	delete velocities;
}
void ComputeShaderParticles::update()
{
	glDispatchCompute(numOfParticles / 1000, 1, 1);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}
void ComputeShaderParticles::draw()
{
	//glEnable(GL_POINT_SPRITE);
	glPointSize(1);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glDrawArrays(GL_POINTS, 0, numOfParticles);
	glDisable(GL_BLEND);

	//for (int i = 0; i < numOfParticles; i += 4)
//	{
	//	std::cout << positions[i][2] << std::endl;
//	}
	//glBindVertexArray(0);
}