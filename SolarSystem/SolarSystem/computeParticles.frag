#version 430

layout(location = 0) out vec4 frag_Colour;

//in vec4 pos;
in vec4 ex_color;

void main(void)
{
	frag_Colour = ex_color;
}