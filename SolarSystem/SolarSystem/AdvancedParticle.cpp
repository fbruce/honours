#include "AdvancedParticle.h"

AdvancedParticle::AdvancedParticle(GLint numOfParticles)
{
	max = 1.0f;
	min = 0.001f;
	this->numOfParticles = numOfParticles;
	if (numOfParticles <= 0)
		return;

	positions = new GLfloat[numOfParticles * 3];
	velocities = new GLfloat[numOfParticles * 3];
	separation = new GLfloat[numOfParticles * 3];
	cohesion = new GLfloat[numOfParticles * 3];
	alignment = new GLfloat[numOfParticles * 3];

	for (int i = 0; i < numOfParticles * 3; i += 3)
	{
		positions[i] = 1.0f;
		positions[i + 1] = 1.0f;
		positions[i + 2] = 1.0f;
		velocities[i] = -randomNumber->generateRandomNumber(max, min);
		velocities[i + 1] = -randomNumber->generateRandomNumber(max, min);
		velocities[i + 2] = -randomNumber->generateRandomNumber(max, min);
		separation[i] = -randomNumber->generateRandomNumber(max, min);
		separation[i + 1] = -randomNumber->generateRandomNumber(max, min);
		separation[i + 2] = -randomNumber->generateRandomNumber(max, min);
		cohesion[i] = -randomNumber->generateRandomNumber(max, min);
		cohesion[i + 1] = -randomNumber->generateRandomNumber(max, min);
		cohesion[i + 2] = -randomNumber->generateRandomNumber(max, min);
		alignment[i] = -randomNumber->generateRandomNumber(max, min);
		alignment[i + 1]  = -randomNumber->generateRandomNumber(max, min);
		alignment[i + 2] = -randomNumber->generateRandomNumber(max, min);
		//positionArray[i] = positions[i];
	//	positionArray[i + 1] = positions[i + 1];
	//	positionArray[i + 2] = positions[i + 2];
	}
}
void AdvancedParticle::init()
{
	glGenVertexArrays(1, vao);//create 1 VAO to hold VBO's
	glGenBuffers(6, vbo);//create 5 VBO's for particle characteristics
	glBindVertexArray(vao[0]);//bind current VAO

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);//setup VBO for position
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), positions, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), velocities, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), separation, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), cohesion, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(3);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[4]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), alignment, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(4);

}
void AdvancedParticle::draw()
{
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_POINT_SMOOTH);
	glPointSize(3.0f);

	glBindVertexArray(vao[0]);//bind current VAO

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);//setup VBO for position
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), positions, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), velocities, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), separation, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), cohesion, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(3);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[4]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), alignment, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(4);

	glDrawArrays(GL_POINTS, 0, numOfParticles);
	glBindVertexArray(0);
}
void AdvancedParticle::update(Planet *parent)
{

}
void AdvancedParticle::calculateCohesion()
{
	//for each (GLfloat& position in positionArray)
	//{
	//	cout << position << endl;
	//}
}
void AdvancedParticle::calculateAlignment()
{

}
void AdvancedParticle::calculateSeparation()
{

}
GLfloat AdvancedParticle::calculateDistance()
{

	return 0.0f;
}