#ifndef STATENEWTON_H
#define STATENEWTON_H

#include "GameState.h"
#include "rt3d.h"
#include "Camera.h"
#include "GUI.h"
#include "ShaderManager.h"
#include "LoadBitmap.h"
#include "loadCubemap.h"
#include "loadObject.h"
#include "TextureManager.h"
#include "NormalMapping.h"
#include "Planet.h"
#include "Timer.h"
#include "GlobalVariables.h"
#include "Moon.h"
#include "PlanetPerturb.h"
#include "RingParticle.h"
#include "AdvancedParticle.h"
#include "NormalMapping.h"
#include "ComputeShaderParticles.h"
#include <stack>
#include <vector>
#include <iostream>
#include <sstream>
#include <cstring>

#define DEG_TO_RADIAN 0.017453293

using namespace GlobalVariables;

class StateNewton : public GameState
{
private:
	Game *context;
	Camera *camera;
	ShaderManager *shaderManager;
	GUI *gui;
	Timer *stateTimer;
	GLuint tex;
	GLuint *basicShader;
	GLuint *texturedShader;
	GLuint *skyboxShader;
	GLuint guiShader;
	GLuint *basicParticles;
	GLuint *ringParticles;
	GLuint *computeParticleShader;//vert and frag
	GLuint *computeShader;//comp
	GLuint *haloParticles;
	LoadBitmap *loadBitmap;
	LoadCubemap *loadCubemap;
	TextureManager *texture;
	NormalMapping *normals;
	LoadObject *sphere;
	LoadObject *cube;
	RingParticle *ring, *ring1, *ring2, *uranusRing, *jupiterRing, *neptuneRing, *sunHalo;
	GLuint textures[10];
	GLuint skybox[5];
	stack<glm::mat4> mvStack;
	GLuint meshObjects[2];
	GLuint sphereIndexCount = 0;
	GLuint cubeIndexCount = 0;
	GLfloat cameraRotation = 0.0f;
	glm::vec3 cameraPosition;
	Planet *sol, *mercury, *venus, *earth, *mars, *jupiter, *saturn,*neptune,*uranus,*pluto;
	//PlanetPerturb *mercury1, *venus1;
	float timeNow = 0.0f;
	GLfloat solarSystemScale = 0.001f;
	Moon *moon;
	GLuint *flocking;
	AdvancedParticle *flockingParticles;
	ComputeShaderParticles *computeParticles;
	glm::mat4 projection;

	

public:
	StateNewton(void);
	~StateNewton(void);
	void init(Game &context);
	bool handleSDLEvent(Game &context, SDL_Event* sdlEvent);
	void update();
	void draw(SDL_Window *window);
	glm::vec3 getViewMatrix() { return cameraPosition; }
	//glm::vec3 Orientate(glm::vec3 xyz);
};

#endif