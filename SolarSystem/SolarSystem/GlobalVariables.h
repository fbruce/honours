#ifndef GLOBALVARIABLES_H
#define GLOBALVARIABLES_H

#include<GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <iostream>
#include <stack>

namespace GlobalVariables
{
		 extern const  GLfloat GravitationalConstant;
		 extern const  GLfloat OverallScale;
		 extern const  GLfloat AstronomicalUnitScale;
		 extern const  GLfloat RadiusScale;
		 extern const GLfloat SunScale;
		 extern const GLfloat GasGiantScale;
		 extern const GLfloat DrawingScale;

		 //extern const  GLfloat sunScale;
		 //extern const  GLfloat mercuryScale;
		 //extern const  GLfloat venusScale;
		 //extern const  GLfloat earthScale;
		 //extern const  GLfloat marsScale;
		 //extern const  GLfloat jupiterScale;
		 //extern const  GLfloat saturnScale;
		 //extern const  GLfloat neptuneScale;
		 //extern const  GLfloat uranusScale;
		 //extern const  GLfloat plutoScale;

		 extern const  GLfloat sunRadius;
		 extern const  GLfloat mercuryRadius;
		 extern const  GLfloat venusRadius;
		 extern const  GLfloat earthRadius;
		 extern const  GLfloat marsRadius;
		 extern const  GLfloat jupiterRadius;
		 extern const  GLfloat saturnRadius;
		 extern const  GLfloat neptuneRadius;
		 extern const  GLfloat uranusRadius;
		 extern const  GLfloat plutoRadius;

		 extern const  GLfloat mercuryOrbitalSpeed;
		 extern const  GLfloat venusOrbitalSpeed;
		 extern const  GLfloat earthOrbitalSpeed;
		 extern const  GLfloat marsOrbitalSpeed;
		 extern const  GLfloat jupiterOrbitalSpeed;
		 extern const  GLfloat saturnOrbitalSpeed;
		 extern const  GLfloat neptuneOrbitalSpeed;
		 extern const  GLfloat uranusOrbitalSpeed;
		 extern const  GLfloat plutoOrbitalSpeed;

		 extern const GLfloat moonOrbitalSpeed;

		 extern const  GLfloat sunRotationalSpeed;
		 extern const  GLfloat mercuryRotationalSpeed;
		 extern const  GLfloat venusRotationalSpeed;
		 extern const  GLfloat earthRotationalSpeed;
		 extern const  GLfloat marsRotationalSpeed;
		 extern const  GLfloat jupiterRotationalSpeed;
		 extern const  GLfloat saturnRotationalSpeed;
		 extern const  GLfloat neptuneRotationalSpeed;
		 extern const  GLfloat uranusRotationalSpeed;
		 extern const  GLfloat plutoRotationalSpeed;

		 extern const GLfloat moonRotationalSpeed;

		 extern const  GLfloat sunMass;
		 extern const  GLfloat mercuryMass;
		 extern const  GLfloat venusMass;
		 extern const  GLfloat earthMass;
		 extern const  GLfloat marsMass;
		 extern const  GLfloat jupiterMass;
		 extern const  GLfloat saturnMass;
		 extern const  GLfloat neptuneMass;
		 extern const  GLfloat uranusMass;
		 extern const  GLfloat plutoMass;

		 extern const  glm::vec3 solStartPositionAU;
		 extern const  glm::vec3 mercuryStartPositionAU;
		 extern const  glm::vec3 venusStartPositionAU;
		 extern const  glm::vec3 earthStartPositionAU;
		 extern const  glm::vec3 marsStartPositionAU;
		 extern const  glm::vec3 jupiterStartPositionAU;
		 extern const  glm::vec3 saturnStartPositionAU;
		 extern const  glm::vec3 uranusStartPositionAU;
		 extern const  glm::vec3 neptuneStartPositionAU;
		 extern const  glm::vec3 plutoStartPositionAU;

		 extern const GLfloat mercuryVenusMinDistance;
		 extern const GLfloat venusEarthMinDistance;

		//orbital speed pre-calculated to save on compute time, 1 year is taken as base
		
		 extern const GLint sunID;
		 extern const GLint mercuryID;
		 extern const GLint venusID;
		 extern const GLint earthID;
		 extern const GLint marsID;
		 extern const GLint jupiterID;
		 extern const GLint saturnID;
		 extern const GLint uranusID;
		 extern const GLint neptuneID;
		 extern const GLint plutoID;
};

#endif