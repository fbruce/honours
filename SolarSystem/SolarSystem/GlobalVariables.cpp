#include "GlobalVariables.h"

namespace GlobalVariables{

	const GLfloat GravitationalConstant = 0.00000000006f;

	const GLfloat RadiusScale = 0.001f;
	const GLfloat GasGiantScale = 0.0001f;
	const GLfloat SunScale = 0.00001f;
	const GLfloat DrawingScale = 30.0f;
	const GLfloat AstronomicalUnitScale = 30.0f;

	const glm::vec3 solStartPositionAU = glm::vec3(0.0f, 0.0f, 0.0f);
	const glm::vec3 mercuryStartPositionAU = glm::vec3(0.39f, 0.0f, 0.39f);
	const glm::vec3 venusStartPositionAU = glm::vec3(0.72f, 0.0f, 0.72f);
	const glm::vec3 earthStartPositionAU = glm::vec3(1.0f, 0.0f, 1.0f);
	const glm::vec3 marsStartPositionAU = glm::vec3(1.52f, 0.0f, 1.52f);
	const glm::vec3 jupiterStartPositionAU = glm::vec3(5.2f, 0.0f, 0.0f);
	const glm::vec3 saturnStartPositionAU = glm::vec3(9.54f, 0.0f, 0.0f);
	const glm::vec3 uranusStartPositionAU = glm::vec3(19.18f, 0.0f, 0.0f);
	const glm::vec3 neptuneStartPositionAU = glm::vec3(30.06f, 0.0f, 0.0f);
	const glm::vec3 plutoStartPositionAU = glm::vec3(50.0f, 0.0f, 0.0f);
	
	const GLfloat sunRadius = 695000.0f;
	const GLfloat mercuryRadius = 2440.0f;
	const GLfloat venusRadius = 6052.0f;
	const GLfloat earthRadius = 6378.0f;
	const GLfloat marsRadius = 3397.0f;
	const GLfloat jupiterRadius = 71492.0f;
	const GLfloat saturnRadius = 60268.0f;
	const GLfloat neptuneRadius = 24766.0f;
	const GLfloat uranusRadius = 25559.0f;
	const GLfloat plutoRadius = 1150.0f;

	const GLfloat sunOrbitalSpeed = 0.0f;
	const GLfloat mercuryOrbitalSpeed = 4.1f;
	const GLfloat venusOrbitalSpeed = 1.6f;
	const GLfloat earthOrbitalSpeed = 1.0f;
	const GLfloat marsOrbitalSpeed = 0.98f;
	const GLfloat jupiterOrbitalSpeed = 0.08f;
	const GLfloat saturnOrbitalSpeed = 0.03f;
	const GLfloat neptuneOrbitalSpeed = 0.001f;
	const GLfloat uranusOrbitalSpeed = 0.006f;
	const GLfloat plutoOrbitalSpeed = 0.0045f;

	const GLfloat moonOrbitalSpeed = 10.0f;

	const GLfloat sunRotationalSpeed = 0.04f;
	const GLfloat mercuryRotationalSpeed = 0.02f;
	const GLfloat venusRotationalSpeed = 0.004f;
	const GLfloat earthRotationalSpeed = 1.0f;
	const GLfloat marsRotationalSpeed = 0.04f;
	const GLfloat jupiterRotationalSpeed = 2.0f;
	const GLfloat saturnRotationalSpeed = 2.5f;
	const GLfloat neptuneRotationalSpeed = 1.4f;
	const GLfloat uranusRotationalSpeed = 1.7f;
	const GLfloat plutoRotationalSpeed = 0.2f;

	const GLfloat moonRotationalSpeed = 0.25f;

	const GLfloat sunMass = 1989550000.0f;
	const GLfloat mercuryMass = 330.2f;
	const GLfloat venusMass = 4868.5f;
	const GLfloat earthMass = 5973.6f;
	const GLfloat marsMass = 641.85f;
	const GLfloat jupiterMass = 1898600.0f;
	const GLfloat saturnMass = 568460.0f;
	const GLfloat neptuneMass = 102430.0f;
	const GLfloat uranusMass = 86832.0f;
	const GLfloat plutoMass = 13.12f;

	//distance in AU
	const GLfloat mercuryVenusMinDistance = 0.6f;//min 0.33
	const GLfloat venusEarthMinDistance = 0.4f;//min 0.28

	//ID for planets
	const GLint sunID = 0;
	const GLint mercuryID = 1;
	const GLint venusID = 2;
	const GLint earthID = 3;
	const GLint marsID = 4;
	const GLint jupiterID = 5;
	const GLint saturnID = 6;
	const GLint uranusID = 7;
	const GLint neptuneID = 8;
	const GLint plutoID = 9;


}