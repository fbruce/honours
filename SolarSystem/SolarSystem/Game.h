#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include <iostream>
#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "GameState.h"
#include "StateMainMenu.h"
#include "StateNewton.h"
//#include "StateRelativity.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

class Game
{
private:
	SDL_Window *window;
	SDL_GLContext glContext;
	GameState * mainMenu;
	GameState * newtonianPhysics;
	GameState * currentState;
	GameState * basicRelativity;

public:
	Game(void);
	~Game(void);
	SDL_Window *setupRC(SDL_GLContext &context);

	void init();
	void run();
	void draw(SDL_Window *window);

	void setState(GameState *newState);
	GameState *getState(void);
	GameState *getMainMenuState(void);
	GameState *getNewtonianPhysicsState(void);
	GameState *getBasicRelativityState(void);

};

#endif