#include "RingParticle.h"

RingParticle::RingParticle(GLint numOfParticles, Planet *planet, GLint scale)
{
	this->numOfParticles = numOfParticles;
	if (numOfParticles <= 0)
		return;
	std::srand(std::time(0));
	positions = new GLfloat[numOfParticles * 3];
	velocities = new GLfloat[numOfParticles * 3];
	ringScale = scale;

	for (int i = 0; i < numOfParticles * 3; i+=3)
	{
		//GLint ID = planet->getID();
		//if (ID == GlobalVariables::saturnID || GlobalVariables::uranusID)
		//{
			glm::vec3 startPosition = glm::vec3(0.0f, 0.0f, 0.0f);
			startPosition = planet->getStartPosition();
			//set minimum and maximum X positions
			GLfloat xMin = startPosition.x - 3.0f;
			GLfloat xMax = startPosition.x + 3.0f;
			//set minimum and maximum Z positions
			GLfloat zMin = startPosition.z - 3.0f;
			GLfloat zMax = startPosition.z + 3.0f;

			positions[i] = -startPosition.x;// / GlobalVariables::DrawingScale;
			//cout << "X" << positions[i] << endl;
			positions[i + 1] = -startPosition.y;// / GlobalVariables::DrawingScale;
			//cout << "Y" << positions[i + 1] << endl;
			positions[i + 2] = -startPosition.z;// / GlobalVariables::DrawingScale;
			//cout << "Z" << positions[i + 2] << endl;

			//positions[i] = ((-startPosition.x) + (R+ r*cos(theta) * cos(omega)));// startPosition.x;
			//cout << "X" << positions[i] << endl;
			//positions[i + 1] = (-startPosition.y + (r * sin(theta))); //((-startPosition.y) + (R + cos(theta) * sin(omega))); //startPosition.y;//0
			//cout << "Y"<< positions[i + 1] << endl;
			//positions[i + 2] = ((-startPosition.z) + (R + cos(theta) * sin(omega))); //startPosition.y;//0(-startPosition.z  + (r * sin(theta)));// startPosition.z;
			//cout <<"Z"<< positions[i + 2] << endl;
			velocities[i] = 0.0f;// float((rand() % 1) + 0.1f);
			velocities[i + 1] = 0.0f;// 0.001f; //(std::rand() % 100 - 50) / 5000.0f;
			velocities[i + 2] = 0.0f;// float((rand() % 1) + 0.1f);// 0.0f; // 0.001f;// (std::rand() % 100 - 50) / 5000.0f;
		
		//}
		//else
		//	return;
	}

}

void RingParticle::init()
{
	glGenVertexArrays(1, vao);//create 1 VAO to hold VBO's
	glGenBuffers(2, vbo);//create 5 VBO's for particle characteristics
	glBindVertexArray(vao[0]);//bind current VAO

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);//setup VBO for position
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), positions, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), velocities, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	//glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);//setup VBO for lifetime
	//glBufferData(GL_ARRAY_BUFFER, numOfParticles, lifetime, GL_DYNAMIC_DRAW);
	//glVertexAttribPointer(2, 3, numOfParticles * 3 * sizeof(GLfloat), GL_FALSE, 0, 0);
	//glEnableVertexAttribArray(0);

	//glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);//setup VBO for fade
	//glBufferData(GL_ARRAY_BUFFER, numOfParticles, fade, GL_DYNAMIC_DRAW);
	//glVertexAttribPointer(3, 3, numOfParticles * 3 * sizeof(GLfloat), GL_FALSE, 0, 0);
	//glEnableVertexAttribArray(0);

	//glBindBuffer(GL_ARRAY_BUFFER, vbo[4]);//setup VBO for colour
	//glBufferData(GL_ARRAY_BUFFER, numOfParticles, colour, GL_DYNAMIC_DRAW);
	//glVertexAttribPointer(4, 3, numOfParticles * 3 * sizeof(GLfloat), GL_FALSE, 0, 0);
	//glEnableVertexAttribArray(0);
}

void RingParticle::draw()
{
	//glEnable(GL_POINT_SPRITE);
	glEnable(GL_POINT_SMOOTH);
	glPointSize(1.0f);

	//rebind arrays in case data has changed (which it will have when applying velocity)
	glBindVertexArray(vao[0]);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);//setup VBO for position
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), positions, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);//setup VBO for velocity
	glBufferData(GL_ARRAY_BUFFER, numOfParticles * 3 * sizeof(GLfloat), velocities, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glDrawArrays(GL_POINTS, 0, numOfParticles);
	glDisable(GL_BLEND);
	glBindVertexArray(0);
}
void RingParticle::update(Planet* planet)
{

}
void RingParticle::update(Planet *planet, GLfloat Xoffset, GLfloat Zoffset, GLfloat angle1, GLfloat angle2)
{
	glm::vec3 startPosition = glm::vec3(0.0f,0.0f,0.0f);
	//GLfloat scale = 0.0f;
	startPosition.x = planet->getNewPosX();
	//cout << startPosition.x << endl;
	startPosition.z = planet->getNewPosZ();
	//scale = planet->getScale();

	GLfloat centreXPos = -startPosition.x;// -ringScale;
	GLfloat centreYPos = -startPosition.y;
	GLfloat centreZPos = -startPosition.z;// -ringScale;
	//GLfloat pi = 3.14171f;
	//GLint R, r;
	//R = 10;
	//r = 5;
	if (startPosition.x != 0.0 && startPosition.z != 0)
	{
		for (int i = 0; i < numOfParticles * 3; i += 3)
		{
			//this current update places the particle right in the centre of saturn
			//positions[i] = centreXPos;
			//cout << "X" << positions[i] << endl;
			//positions[i + 1] = centreYPos;
			//cout << "Y" << positions[i + 1] << endl;
		//	positions[i + 2] = centreZPos;
			//cout << "Z" << positions[i + 2] << endl;

			//ignore velocity at the moment, make particles into circle shape
			GLfloat startingAngle = 0.0f;

			float random1 = ((float)rand() / (float)RAND_MAX) * M_PI * angle1; //2 = whole circle
			float random2 = ((float)rand() / (float)RAND_MAX) * M_PI * angle2;// *0.0025f; // 0.00025f;//does this mean radius?
			
			//WORKING!!
			positions[i] = (ringScale * 2) * (cos(random2) * sin(random1)) + (centreXPos + Xoffset);
			//cout << "X" << positions[i] << endl;
			positions[i + 1] = (ringScale * 2) * (sin(random1) * sin(random2)) + centreYPos;
			//cout << "Y" << positions[i + 1] << endl;
			positions[i + 2] = (ringScale * 2) * (cos(random1)) + (centreZPos + Zoffset);
			//cout << "z" << positions[i + 2] << endl; 

			////particles in a circle in the centre of the sun
			//positions[i] = -(0.05f + cos(random2) * sin(random1));
			//cout << "X" << positions[i] << endl;
			//positions[i + 1] = cos(random1);
			//cout << "Y" << positions[i + 1] << endl;
			//positions[i + 2] = -(5.25 + sin(random1) * sin(random2));
			//cout << "z" << positions[i + 2] << endl;


			velocities[i] = float((rand() % 1) + 0.1f);
			velocities[i + 1] = 0.0f;// 0.001f; //(std::rand() % 100 - 50) / 5000.0f;
			velocities[i + 2] = float((rand() % 1) + 0.1f);// 0.0f; // 0.001f;// (std::rand() % 100 - 50) / 5000.0f;


			///*
			//particle to spawn somewhere within a torus shape
			//move torus to around planet

			//*/
			//GLfloat xMaxLimit = -startPosition.x -scale;//maximum X
			//GLfloat xMinLimit = -startPosition.x +scale;//minimum X
			//GLfloat zMaxLimit = -startPosition.z -scale;// maximum Z
			//GLfloat zMinLimit = -startPosition.z +scale;//minimum Z

			////if (theta < 360 || omega < 360)
			////{
			////	GLint R, r;
			////	R = 10;
			////	r = 5;
			//positions[i] = glm::mix(xMinLimit, xMaxLimit, randFloat());// -startPosition.x - scale;// (R + r*cos(theta))*cos(omega);// (-startPosition.x) +sin(theta) * cos(omega);// (-startPosition.x*GlobalVariables::GasGiantScale) + (R + r * cos(theta) * cos(omega));
			//cout << " X pos "<< positions[i] << endl;
			//positions[i + 1] = 0.0f;// (R + r*cos(theta)* sin(omega));//  (-startPosition.y) +sin(theta) * sin(omega);// (startPosition.y * GlobalVariables::GasGiantScale) + (R + r*cos(theta) * sin(omega));
			//positions[i + 2] = glm::mix(zMinLimit, zMaxLimit, randFloat());// -startPosition.z - scale;// r * sin(theta);//cos(theta); (-startPosition.z) +// (-startPosition.z * GlobalVariables::GasGiantScale) + r * sin(theta);
			////cout << "X" << positions[i] << endl;

			////cout << "Z" << positions[i + 2] << endl;
			////	theta += 0.26f;
			////	omega += 0.62f;
			////}
			////else
			////{
			////	theta = 0.0f;
			////	omega = 0.0f;
			////}


			////positions[i + 1] = 0.0f + rand() / (RAND_MAX / (0.1f - 0.0f));
			////velocities[i] = (R + r*cos(theta))*cos(omega); // 1.0f + rand() / (RAND_MAX / (1.0f - (-1.0f)));//0.0
			//velocities[i] = 0.1f;
			////velocities[i + 1] = (R + r*cos(theta)* sin(omega));/// 0.0f; // 0.001f + rand() / (RAND_MAX / (0.01f - 0.001f));
			//velocities[i + 1] = 0.1f;
			//velocities[i + 2] = 0.1f;

			////velocities[i + 2] = r * sin(theta);// 0.0f; // 1.0f + rand() / (RAND_MAX / (1.1f - (-1.0f)));


			//theta += 0.0026f;
			//omega += 0.0062f;
			////cout << "X" << startPosition.x << endl;
			////cout << "Y" << startPosition.y << endl;
			////cout << "Z" << startPosition.z << endl;
			////cout << "Velocities" << velocities[i+1] << endl;
		}
	}
	else
	{
		startPosition = planet->getStartPosition();

		for (int i = 0; i < numOfParticles * 3; i += 3)
		{
			positions[i] = -startPosition.x;
			positions[i + 1] = -startPosition.y;
			positions[i + 2] = -startPosition.z;
		}

	}

}
void RingParticle::updateVertical(Planet *planet)
{
	glm::vec3 startPosition = glm::vec3(0.0f, 0.0f, 0.0f);
	startPosition.x = planet->getNewPosX();
	startPosition.z = planet->getNewPosZ();

	GLfloat centreXPos = -startPosition.x;// -ringScale;
	GLfloat centreYPos = -startPosition.y;
	GLfloat centreZPos = -startPosition.z;// -ringScale;

	if (startPosition.x != 0.0 && startPosition.z != 0)
	{
		for (int i = 0; i < numOfParticles * 3; i += 3)
		{

			//ignore velocity at the moment, make particles into circle shape

			float random1 = ((float)rand() / (float)RAND_MAX) * M_PI * 2.0f; //2 = whole circle
			float random2 = ((float)rand() / (float)RAND_MAX) * M_PI * 0.05f;// *0.0025f; // 0.00025f;//does this mean radius?
			//float random1 = ((float)rand() / (float)RAND_MAX) * M_PI * 2.0f; //2 = whole circle
			//float random2 = ((float)rand() / (float)RAND_MAX) * M_PI * 0.05f;// *0.0025f; // 0.00025f;//does this mean radius?

			GLfloat posZ = centreZPos + 2.0f;
			//WORKING!!
			positions[i] = ((ringScale * 1.5) * (cos(random2) * sin(random1))) + (centreXPos - 2.5f);
			//cout << "X" << positions[i] << endl;
			positions[i + 1] = (ringScale * 1.5) * (cos(random1)) + (centreYPos); 
			//cout << "Y" << positions[i + 1] << endl;
			positions[i + 2] = ((ringScale * 1.5) * (sin(random1) * sin(random2))) + (centreZPos - 7.5);
			//cout << "z" << positions[i + 2] << endl; 

			//	velocities[i] = float((rand() % 1) + 0.1f);
			//velocities[i + 1] = 0.0f;// 0.001f; //(std::rand() % 100 - 50) / 5000.0f;
			//	velocities[i + 2] = float((rand() % 1) + 0.1f);// 0.0f; // 0.001f;// (std::rand() % 100 - 50) / 5000.0f;
			//startingAngle += 10.0f;
		}
		//else
		//{
		//	startingAngle = 0.0f;
		//}
	}
		else
		{
		startPosition = planet->getStartPosition();

		for (int i = 0; i < numOfParticles * 3; i += 3)
		{
			positions[i] = -startPosition.x;
			positions[i + 1] = -startPosition.y;
			positions[i + 2] = -startPosition.z;
		}

	}

}
void RingParticle::updateHalo(Planet *planet, GLfloat Xoffset, GLfloat Zoffset, GLfloat angle1, GLfloat angle2)
{
	glm::vec3 startPosition = glm::vec3(0.0f, 0.0f, 0.0f);
	//GLfloat scale = 0.0f;
	startPosition.x = planet->getNewPosX();
	//cout << startPosition.x << endl;
	startPosition.z = planet->getNewPosZ();
	//scale = planet->getScale();

	GLfloat centreXPos = -startPosition.x;// -ringScale;
	GLfloat centreYPos = -startPosition.y;
	GLfloat centreZPos = -startPosition.z;// -ringScale;
	//GLfloat pi = 3.14171f;
	//GLint R, r;
	//R = 10;
	//r = 5;
	if (startPosition.x != 0.0 && startPosition.z != 0)
	{
		for (int i = 0; i < numOfParticles * 3; i += 3)
		{
			//this current update places the particle right in the centre of saturn
			//positions[i] = centreXPos;
			//cout << "X" << positions[i] << endl;
			//positions[i + 1] = centreYPos;
			//cout << "Y" << positions[i + 1] << endl;
			//	positions[i + 2] = centreZPos;
			//cout << "Z" << positions[i + 2] << endl;

			//ignore velocity at the moment, make particles into circle shape
			GLfloat startingAngle = 0.0f;

			float random1 = ((float)rand() / (float)RAND_MAX) * M_PI * angle1; //2 = whole circle
			float random2 = ((float)rand() / (float)RAND_MAX) * M_PI * angle2;// *0.0025f; // 0.00025f;//does this mean radius?

			//WORKING!!
			positions[i] = (ringScale * 2) * (cos(random2) * sin(random1)) + (centreXPos + Xoffset);
			//cout << "X" << positions[i] << endl;
			positions[i + 1] = (ringScale * 2) * (sin(random1) * sin(random2)) + centreYPos;
			//cout << "Y" << positions[i + 1] << endl;
			positions[i + 2] = (ringScale * 2) * (cos(random1)) + (centreZPos + Zoffset);
			//cout << "z" << positions[i + 2] << endl; 

			////particles in a circle in the centre of the sun
			//positions[i] = -(0.05f + cos(random2) * sin(random1));
			//cout << "X" << positions[i] << endl;
			//positions[i + 1] = cos(random1);
			//cout << "Y" << positions[i + 1] << endl;
			//positions[i + 2] = -(5.25 + sin(random1) * sin(random2));
			//cout << "z" << positions[i + 2] << endl;


			velocities[i] = 10.0f;// float((rand() % 1) + 0.1f);
			velocities[i + 1] = 10.0f;// 0.001f; //(std::rand() % 100 - 50) / 5000.0f;
			velocities[i + 2] = 10.0f;// float((rand() % 1) + 0.1f);// 0.0f; // 0.001f;// (std::rand() % 100 - 50) / 5000.0f;


			///*
			//particle to spawn somewhere within a torus shape
			//move torus to around planet

			//*/
			//GLfloat xMaxLimit = -startPosition.x -scale;//maximum X
			//GLfloat xMinLimit = -startPosition.x +scale;//minimum X
			//GLfloat zMaxLimit = -startPosition.z -scale;// maximum Z
			//GLfloat zMinLimit = -startPosition.z +scale;//minimum Z

			////if (theta < 360 || omega < 360)
			////{
			////	GLint R, r;
			////	R = 10;
			////	r = 5;
			//positions[i] = glm::mix(xMinLimit, xMaxLimit, randFloat());// -startPosition.x - scale;// (R + r*cos(theta))*cos(omega);// (-startPosition.x) +sin(theta) * cos(omega);// (-startPosition.x*GlobalVariables::GasGiantScale) + (R + r * cos(theta) * cos(omega));
			//cout << " X pos "<< positions[i] << endl;
			//positions[i + 1] = 0.0f;// (R + r*cos(theta)* sin(omega));//  (-startPosition.y) +sin(theta) * sin(omega);// (startPosition.y * GlobalVariables::GasGiantScale) + (R + r*cos(theta) * sin(omega));
			//positions[i + 2] = glm::mix(zMinLimit, zMaxLimit, randFloat());// -startPosition.z - scale;// r * sin(theta);//cos(theta); (-startPosition.z) +// (-startPosition.z * GlobalVariables::GasGiantScale) + r * sin(theta);
			////cout << "X" << positions[i] << endl;

			////cout << "Z" << positions[i + 2] << endl;
			////	theta += 0.26f;
			////	omega += 0.62f;
			////}
			////else
			////{
			////	theta = 0.0f;
			////	omega = 0.0f;
			////}


			////positions[i + 1] = 0.0f + rand() / (RAND_MAX / (0.1f - 0.0f));
			////velocities[i] = (R + r*cos(theta))*cos(omega); // 1.0f + rand() / (RAND_MAX / (1.0f - (-1.0f)));//0.0
			//velocities[i] = 0.1f;
			////velocities[i + 1] = (R + r*cos(theta)* sin(omega));/// 0.0f; // 0.001f + rand() / (RAND_MAX / (0.01f - 0.001f));
			//velocities[i + 1] = 0.1f;
			//velocities[i + 2] = 0.1f;

			////velocities[i + 2] = r * sin(theta);// 0.0f; // 1.0f + rand() / (RAND_MAX / (1.1f - (-1.0f)));


			//theta += 0.0026f;
			//omega += 0.0062f;
			////cout << "X" << startPosition.x << endl;
			////cout << "Y" << startPosition.y << endl;
			////cout << "Z" << startPosition.z << endl;
			////cout << "Velocities" << velocities[i+1] << endl;
		}
	}
	else
	{
		startPosition = planet->getStartPosition();

		for (int i = 0; i < numOfParticles * 3; i += 3)
		{
			positions[i] = -startPosition.x;
			positions[i + 1] = -startPosition.y;
			positions[i + 2] = -startPosition.z;
		}

	}

}
void RingParticle::createTorus(GLfloat startPosX, GLfloat startPosY, GLfloat startPosZ, GLfloat outerRadius, GLfloat innerRadius)
{
	GLfloat R = outerRadius;
	GLfloat r = innerRadius; 
	GLfloat x = startPosX;
	GLfloat y = startPosY;
	GLfloat z = startPosZ;
}