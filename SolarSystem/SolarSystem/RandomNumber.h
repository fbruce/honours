#ifndef RANDOMNUMBER_H
#define RANDOMNUMBER_H

#include<GL/glew.h>
#include <cstdlib>

class RandomNumber
{
public:
	RandomNumber();
	~RandomNumber(void){ delete this; }
	GLfloat generateRandomNumber(GLfloat min, GLfloat max);

private:
	GLfloat min, max;

};
#endif