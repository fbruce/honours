#ifndef NORMALMAPPING_H
#define NORMALMAPPING_H

#include "ShaderManager.h"
//#include "StateNewton.h"
#include "loadObject.h"
#include "LoadBitmap.h"
#include "rt3d.h"
#include "TextureManager.h"
#include "Camera.h"
#include "GL\glew.h"

class NormalMapping
{
private:
	vector<GLfloat> verts;
	vector<GLfloat> norms;
	vector<GLfloat> tex_coords;
	vector<GLuint> indices;
	vector<GLfloat> tangents;
	GLuint *normalShader;
	float attConstant = 1.0f;
	float attLinear = 0.0f;
	float attQuadratic = 0.005f;
	float theta = 0.0f;
	glm::vec4 lightPos;
	stack<glm::mat4> mvStack;
	TextureManager *normals;
	ShaderManager *shaders;
	Camera *camera;
	LoadObject *object;
	rt3d::lightStruct light0;
	rt3d::materialStruct material0;
	rt3d::materialStruct material1;
public:
	NormalMapping(void);
	~NormalMapping(void);
	void calculateTangents(vector<GLfloat> &tangents, vector<GLfloat> &verts, vector<GLfloat> &normals, vector<GLfloat> &tex_coords, vector<GLuint> &indices);
	vector<GLfloat> getTangets(){ return tangents; }
	void init();
	void draw();
	//draw data
};
#endif 
/*
Include external class in here, read in objects from State Newton
Read in details about object from ObjectLoader
Set VBO for tangents here
Set uniforms for normal shader here
deal with lighting and material stuff here



*/