#include "StateMainMenu.h"
#include "Game.h"

StateMainMenu::StateMainMenu(void)
{
	shader = new ShaderManager;//this needs to be here or shadermanager is never initialised and can't run
	menu = new GUI;
}
StateMainMenu::~StateMainMenu(void)
{
}
void StateMainMenu::init(Game &context)
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	shader->init();//move initialization to shader class and get and set variables for shaders from there? 
	shaderID = *shader->getGuiShaderProgram();//save shaders in shader manager
}
void StateMainMenu::draw(SDL_Window *window)
{

	glm::mat4 projection(1.0f);
	projection = glm::perspective(60.0f, 800.0f / 600.0f, 1.0f, 200.0f);
	glUseProgram(shaderID);
	rt3d::setUniformMatrix4fv(shaderID, "projection", glm::value_ptr(projection));

	//glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	menu->textToTexture("Main Menu");
	menu->draw(-0.2f, 0.6f, shaderID);

	menu->textToTexture("The Solar System");
	menu->draw(-0.2f, 0.4f, shaderID);

	menu->textToTexture("Press Space to start");
	menu->draw(-0.2f, 0.2f, shaderID);

	menu->textToTexture("Press Esc to exit");
	menu->draw(-0.2f, 0.0f, shaderID);

	glDisable(GL_BLEND);

	printf("%s\n", glGetString(GL_VERSION));

	SDL_GL_SwapWindow(window);
}
bool StateMainMenu::handleSDLEvent(Game &context, SDL_Event *sdlEvent)
{
	const Uint8 *keys = SDL_GetKeyboardState(NULL);

	if (keys[SDL_SCANCODE_SPACE]) context.setState(context.getNewtonianPhysicsState());
	if (keys[SDL_SCANCODE_ESCAPE]) exit(0);

	return true;
}
void StateMainMenu::update()
{
	//no update needed in main menu state
}