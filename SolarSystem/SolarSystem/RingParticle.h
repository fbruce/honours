#ifndef RINGPARTICLE_H
#define RINGPARTICLE_H

#include "Particle.h"
#include<GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <cstdlib>
#include "ctime"
#include "GlobalVariables.h"
#include "Planet.h"

using namespace GlobalVariables;

class RingParticle : public Particle
{
	/*
	Ring particles don't need lifetime or fade, just need to stay within certain coordinates
	Need to set maximum radius and minimum radius from a point
	Need to retrieve planet position from Planet for Saturn, will be done by global variable
	*/
	private:
		//glm::vec3 position;
		//glm::vec3 velocity;
		GLfloat *positions;
		GLfloat *velocities;
		GLuint vao[1];
		GLuint vbo[3];
		GLint numOfParticles;
		GLint ringScale;
	public:
		RingParticle(GLint numOfParticles, Planet *planet, GLint scale);
		~RingParticle(void){ delete this; }
		GLfloat getPositions(){ return *positions; }
		GLfloat getVelocities(){ return *velocities; }
		void init(void);
		void update(Planet *planet);
		void update(Planet *planet, GLfloat Xoffset, GLfloat Zoffset, GLfloat angle1, GLfloat angle2);
		void updateVertical(Planet *planet);
		void updateHalo(Planet *planet, GLfloat Xoffset, GLfloat Zoffset, GLfloat angle1, GLfloat angle2);
		void draw();
		GLfloat randFloat(){ return rand() / (RAND_MAX / (1.0f - (0.0f))); }
		void createTorus(GLfloat startPosX, GLfloat startPosY, GLfloat startPosZ, GLfloat outerRadius, GLfloat innerRadius);
};

#endif