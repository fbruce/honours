#ifndef ADVANCEDPARTICLE_H
#define ADVANCEDPARTICLE_H

#include "Particle.h"
#include "RandomNumber.h"
#include "Planet.h"
#include<GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace GlobalVariables;

class AdvancedParticle : public Particle
{

public:
	AdvancedParticle(GLint numOfParticles);
	~AdvancedParticle(void){ delete this; }
	void init(void);
	void update(Planet *parent);
	void draw();
	void calculateCohesion();
	void calculateAlignment();
	void calculateSeparation();
	GLfloat calculateDistance();

private:
	GLuint vao[1];
	GLuint vbo[6];
	GLint numOfParticles;
	GLfloat *positions;
	GLfloat *velocities;
	//GLfloat *fade;
	//GLfloat *lifeTime;
	GLfloat *cohesion;
	GLfloat *separation;
	GLfloat *alignment;
	GLint *particleNum;
	RandomNumber *randomNumber;
	GLfloat min, max;
	//float positionArray[];
};

#endif
