#include "Game.h"

Game::Game(void)
{
	mainMenu = new StateMainMenu();
	newtonianPhysics = new StateNewton();
	currentState = mainMenu;
}
Game::~Game(void)
{

}
GameState *Game::getMainMenuState(void)
{
	return mainMenu;
}
GameState *Game::getNewtonianPhysicsState(void)
{
	return newtonianPhysics;
}
//GameState *Game::getBasicRelativityState(void)
//{
	//return basicRelativity;
//}
void Game::setState(GameState* newState)
{
	if (newState != currentState)
	{
		currentState = newState;
		currentState->init(*this);
	}
}
SDL_Window * Game::setupRC(SDL_GLContext &context) {
	SDL_Window * window;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
		rt3d::exitFatalError("Unable to initialize SDL");

	// Request an OpenGL 3.0 context.

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);//3
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);//4
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)

	// Create 1280x720 window
	window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		1280, 720, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!window) // Check window was created OK
		rt3d::exitFatalError("Unable to create window");

	context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
	SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}
void Game::init()
{
	window = setupRC(glContext); // Create window and render context 
	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << std::endl;
		exit(1);
	}
	//set shaders up here

	currentState->init(*this);
}
void Game::draw(SDL_Window * window)
{
	currentState->draw(window);
}
void Game::run()
{
	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				//running = false;
				exit(0);
			else
				running = currentState->handleSDLEvent(*this, &sdlEvent);//may fold this into update
		}

		currentState->update();
		currentState->draw(window);
	}

	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
	SDL_Quit();
}