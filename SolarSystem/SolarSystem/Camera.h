#ifndef CAMERA_H
#define CAMERA_H

#include <GL\glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL.h>
#include <iostream>

#define DEG_TO_RADIAN 0.017453293

class Camera
{
private:
	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;
	GLfloat r = 0.0f;
	GLfloat y = 0.0f;

public:
	Camera(void);
	~Camera(void);
	void SetAt(glm::vec3 cameraAim){ at = cameraAim; }//sets where the camera is pointing at
	void SetEye(glm::vec3 pos, GLfloat angle, GLfloat d){ eye = moveForward(pos, angle, d); }
	glm::vec3 getEye(){ return eye; }
	glm::mat4 getLookAt(){ return glm::lookAt(eye, at, up); }
	glm::vec3 moveForward(glm::vec3 pos, GLfloat angle, GLfloat d);
	glm::vec3 moveRight(glm::vec3 pos, GLfloat angle, GLfloat d);
	glm::vec3 moveUp(glm::vec3 pos);
	glm::vec3 moveDown(glm::vec3 pos);
	glm::vec3 rotateCamera(glm::vec3 pos, GLfloat angle, GLfloat d);
	glm::vec3 getAt(){ return at; }
	//glm::vec3 *getEye(){ return &eye; }
	//glm::vec3 *getAt(){ return &at; }
	//glm::vec3 *getUp(){ return &up; }
	////glm::mat4 getLookAt(){ return glm::lookAt(getEye(), at, up);}
	//void setAt(glm::vec3 aim){ at = aim; }
	//void setEye(glm::vec3 pos, GLfloat angle, GLfloat d){ eye = moveForward(pos, angle, d); }
	//void setUp(glm::vec3 up);


};


#endif