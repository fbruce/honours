#ifndef STATEMAINMENU_H
#define STATEMAINMENU_H

#include "GameState.h"
#include <GL/glew.h>
#include "ShaderManager.h"
#include "rt3d.h"
#include "GUI.h"

class StateMainMenu : public GameState
{
private:
	Game *context;
	GUI *menu;
	ShaderManager *shader;
	GLuint shaderID;
public:
	StateMainMenu(void);
	~StateMainMenu(void);
	void init(Game &context);
	bool handleSDLEvent(Game &context, SDL_Event* sdlEvent);
	void update();
	void draw(SDL_Window *window);
};
#endif