#ifndef ASTEROID_H
#define ASTEROID_H

#include "Body.h"
#include "GL/glew.h"

class Asteroid : public Body
{
public:
	GLfloat radius;
private:
	Asteroid(void);
	~Asteroid(void);
	float rotation();
	float orbit();
	void init();
	void draw();
	void detectCollision();
};


#endif