#include "StateNewton.h"
#include "Game.h"


StateNewton::StateNewton(void)
{
	camera = new Camera();
	shaderManager = new ShaderManager();
	loadBitmap = new LoadBitmap();
	loadCubemap = new LoadCubemap();
	gui = new GUI();
	cameraPosition = glm::vec3(-30.0f, 0.0f, 20.0f);
	texture = new TextureManager();
	stateTimer = new Timer();
	projection = glm::mat4(1.0f);
}
StateNewton::~StateNewton(void)
{

}
void StateNewton::init(Game &context)
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	texture->init();
	stateTimer->init();

	//setup cubemap
	const char *skyboxTexFiles[6] = { "SpaceSkyBox/black.bmp", "SpaceSkyBox/black.bmp",
		"SpaceSkyBox/black.bmp", "SpaceSkyBox/black.bmp",
		"SpaceSkyBox/black.bmp", "SpaceSkyBox/black.bmp" };
	loadCubemap->loadcubemap(skyboxTexFiles, &skybox[0]);


	//set up shaders
	shaderManager->init();
	basicShader = shaderManager->getBasicShaderProgram();
	texturedShader = shaderManager->getTexturedShaderProgram();
	skyboxShader = shaderManager->getSkyboxShaderProgram();
	guiShader = *shaderManager->getGuiShaderProgram();
	basicParticles = shaderManager->getBasicParticleProgram();
	ringParticles = shaderManager->getRingParticleProgram();
	flocking = shaderManager->getFlockingProgram();
	computeShader = shaderManager->getComputeShader();//comp
	computeParticleShader = shaderManager->getComputeParticlesProgram();//frag and vert
	haloParticles = shaderManager->getHaloShaderProgram();

	//set camera start position
	cameraPosition = glm::vec3(50.0f, 0.0f, -200.0f);// 0.0 0.0 -300

	//load Objects
	sphere = new LoadObject("sphere.obj");
	sphereIndexCount = sphere->getindexCount();
	meshObjects[0] = rt3d::createMesh(sphere->getVertsData().size() / 3, sphere->getVertsData().data(), nullptr, sphere->getNorms().data(), sphere->getTex().data(), sphereIndexCount, sphere->getIndices().data());
	cube = new LoadObject("cube.obj");
	cubeIndexCount = cube->getindexCount();
	meshObjects[1] = rt3d::createMesh(cube->getVertsData().size() / 3, cube->getVertsData().data(), nullptr, cube->getNorms().data(), cube->getTex().data(), cubeIndexCount, cube->getIndices().data());
	//create planets
	sol = new Planet(GlobalVariables::sunID, glm::vec3(0.0f,0.0f,0.0f), GlobalVariables::sunRadius, GlobalVariables::sunRadius * GlobalVariables::SunScale, meshObjects[0], sphereIndexCount, GlobalVariables::sunMass, 0.0f, GlobalVariables::sunRotationalSpeed);
	mercury = new Planet(GlobalVariables::mercuryID, GlobalVariables::mercuryStartPositionAU * GlobalVariables::DrawingScale, GlobalVariables::mercuryRadius, GlobalVariables::mercuryRadius * GlobalVariables::RadiusScale, meshObjects[0], sphereIndexCount, GlobalVariables::mercuryMass, GlobalVariables::mercuryOrbitalSpeed, GlobalVariables::mercuryRotationalSpeed);
	venus = new Planet(GlobalVariables::venusID, GlobalVariables::venusStartPositionAU * GlobalVariables::DrawingScale, GlobalVariables::venusRadius, GlobalVariables::venusRadius * GlobalVariables::RadiusScale, meshObjects[0], sphereIndexCount, GlobalVariables::venusMass, GlobalVariables::venusOrbitalSpeed, GlobalVariables::venusRotationalSpeed);
	earth = new Planet(GlobalVariables::earthID, GlobalVariables::earthStartPositionAU * GlobalVariables::DrawingScale, GlobalVariables::earthRadius, GlobalVariables::earthRadius* GlobalVariables::RadiusScale, meshObjects[0], sphereIndexCount, GlobalVariables::earthMass, GlobalVariables::earthOrbitalSpeed, GlobalVariables::earthRotationalSpeed);
	mars = new Planet(GlobalVariables::marsID, GlobalVariables::marsStartPositionAU * GlobalVariables::DrawingScale, GlobalVariables::marsRadius, GlobalVariables::marsRadius* GlobalVariables::RadiusScale, meshObjects[0], sphereIndexCount, GlobalVariables::marsMass, GlobalVariables::marsOrbitalSpeed, GlobalVariables::marsRotationalSpeed);
	jupiter = new Planet(GlobalVariables::jupiterID, GlobalVariables::jupiterStartPositionAU * GlobalVariables::DrawingScale, GlobalVariables::jupiterRadius, GlobalVariables::jupiterRadius* GlobalVariables::GasGiantScale, meshObjects[0], sphereIndexCount, GlobalVariables::jupiterMass, GlobalVariables::jupiterOrbitalSpeed, GlobalVariables::jupiterRotationalSpeed);
	saturn = new Planet(GlobalVariables::saturnID, GlobalVariables::saturnStartPositionAU * GlobalVariables::DrawingScale, GlobalVariables::saturnRadius, GlobalVariables::saturnRadius* GlobalVariables::GasGiantScale, meshObjects[0], sphereIndexCount, GlobalVariables::saturnMass, GlobalVariables::saturnOrbitalSpeed, GlobalVariables::saturnRotationalSpeed);
	uranus = new Planet(GlobalVariables::uranusID, GlobalVariables::uranusStartPositionAU * GlobalVariables::DrawingScale, GlobalVariables::uranusRadius, GlobalVariables::uranusRadius* GlobalVariables::GasGiantScale, meshObjects[0], sphereIndexCount, GlobalVariables::uranusMass, GlobalVariables::uranusOrbitalSpeed, GlobalVariables::uranusRotationalSpeed);
	neptune = new Planet(GlobalVariables::neptuneID, GlobalVariables::neptuneStartPositionAU * GlobalVariables::DrawingScale, GlobalVariables::neptuneRadius, GlobalVariables::neptuneRadius* GlobalVariables::GasGiantScale, meshObjects[0], sphereIndexCount, GlobalVariables::neptuneMass, GlobalVariables::neptuneOrbitalSpeed, GlobalVariables::neptuneRotationalSpeed);
	pluto = new Planet(GlobalVariables::plutoID, GlobalVariables::plutoStartPositionAU * GlobalVariables::DrawingScale, GlobalVariables::plutoRadius, GlobalVariables::plutoRadius* GlobalVariables::RadiusScale, meshObjects[0], sphereIndexCount, GlobalVariables::plutoMass, GlobalVariables::plutoOrbitalSpeed, GlobalVariables::plutoRotationalSpeed);
	
	moon = new Moon(earth, GlobalVariables::earthStartPositionAU * GlobalVariables::DrawingScale, 5.0f, meshObjects[0], sphereIndexCount, 1.0f, GlobalVariables::moonOrbitalSpeed, GlobalVariables::moonRotationalSpeed, 1.0f);
	//normals->calculateTangents(normals->getTangets(), sphere->getVertsData(), sphere->getNorms(), sphere->getTex(), sphere->getIndices());
	//normals->init();
	ring = new RingParticle(50, saturn, 5);
	ring1 = new RingParticle(50, saturn, 6);
	ring2 = new RingParticle(50, saturn, 7);

	ring->init();
	ring1->init();
	ring2->init();

	uranusRing = new RingParticle(100, uranus, 2);
	uranusRing->init();

	jupiterRing = new RingParticle(50, jupiter, 5);
	jupiterRing->init();

	neptuneRing = new RingParticle(50, neptune, 2);
	neptuneRing->init();

	sunHalo = new RingParticle(1000, sol, 4);
	sunHalo->init();

	flockingParticles = new AdvancedParticle(50);
	flockingParticles->init();

	projection = glm::perspective(60.0f, 800.0f / 600.0f, 1.0f, 5000.0f);

	//glUseProgram(*computeParticleShader);
	//rt3d::setUniformMatrix4fv(*computeParticleShader, "projection", glm::value_ptr(projection));
	//glEnable(GL_POINT_SMOOTH);
	//glUseProgram(*computeShader);
	computeParticles = new ComputeShaderParticles(50000);
	//computeParticles->init();
}
void StateNewton::draw(SDL_Window *window)
{
	//initial setup
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.5f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	timeNow = stateTimer->getCurrentTime();

	//glm::mat4 projection(1.0f);
	//projection = glm::perspective(60.0f, 800.0f / 600.0f, 1.0f, 5000.0f);
	//glUseProgram(*basicShader);
	//rt3d::setUniformMatrix4fv(*basicShader, "projection", glm::value_ptr(projection));

	//set up matrix for drawing, this equals top of stack
	glm::mat4 modelview(1.0f);
	mvStack.push(modelview);
	//camera set up
	camera->SetAt(cameraPosition);
	camera->SetEye(cameraPosition,cameraRotation,5.0f);
	mvStack.top() = camera->getLookAt();

	//draws skybox
	glUseProgram(*skyboxShader);
	rt3d::setUniformMatrix4fv(*skyboxShader, "projection", glm::value_ptr(projection));
	glDepthMask(GL_FALSE); // make sure writing to update depth test is off
	glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top());
	mvStack.push(glm::mat4(mvRotOnlyMat3));
	glCullFace(GL_FRONT);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, skybox[0]);
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(2.0f, 2.0f, 2.0f));
	rt3d::setUniformMatrix4fv(*skyboxShader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[1], cubeIndexCount, GL_TRIANGLES);
	mvStack.pop();

	//reset faces and depth mask
	glCullFace(GL_BACK);
	glDepthMask(GL_TRUE);
	glUseProgram(*texturedShader);//stays for moment

	//reset projection matrix
	rt3d::setUniformMatrix4fv(*texturedShader, "projection", glm::value_ptr(projection));
	//cout << timeNow << endl;
	GLuint tex1 = 0;//sets texture to use
	tex1 = texture->getTexture(3);//gets texture to use
	glBindTexture(GL_TEXTURE_2D, tex1);//sets current texture
	sol->draw(*texturedShader, mvStack.top(), cameraPosition);

	tex1 = texture->getTexture(4);
	glBindTexture(GL_TEXTURE_2D, tex1);
	mercury->draw(*texturedShader, mvStack.top(), cameraPosition);

	tex1 = texture->getTexture(5);
	glBindTexture(GL_TEXTURE_2D, tex1);
	venus->draw(*texturedShader, mvStack.top(), cameraPosition);


	tex1 = texture->getTexture(1);
	glBindTexture(GL_TEXTURE_2D, tex1);
	earth->draw(*texturedShader, mvStack.top(), cameraPosition);

	tex1 = texture->getTexture(2);
	glBindTexture(GL_TEXTURE_2D, tex1);
	moon->draw(*texturedShader, mvStack.top(), earth);

	tex1 = texture->getTexture(6);
	glBindTexture(GL_TEXTURE_2D, tex1);
	mars->draw(*texturedShader, mvStack.top(),cameraPosition);

	tex1 = texture->getTexture(7);
	glBindTexture(GL_TEXTURE_2D, tex1);
	jupiter->draw(*texturedShader, mvStack.top(),cameraPosition);

	tex1 = texture->getTexture(8);
	glBindTexture(GL_TEXTURE_2D, tex1);
	saturn->draw(*texturedShader, mvStack.top(),cameraPosition);

	tex1 = texture->getTexture(9);
	glBindTexture(GL_TEXTURE_2D, tex1);
	uranus->draw(*texturedShader, mvStack.top(),cameraPosition);

	tex1 = texture->getTexture(10);
	glBindTexture(GL_TEXTURE_2D, tex1);
	neptune->draw(*texturedShader, mvStack.top(),cameraPosition);

	tex1 = texture->getTexture(2);
	glBindTexture(GL_TEXTURE_2D, tex1);
	pluto->draw(*texturedShader, mvStack.top(),cameraPosition);


	//particles
	glUseProgram(*ringParticles);
	rt3d::setUniformMatrix4fv(*ringParticles, "projection", glm::value_ptr(projection));
	rt3d::setUniformMatrix4fv(*ringParticles, "modelview", glm::value_ptr(mvStack.top()));
 	ring->draw();
	ring1->draw();
	ring2->draw();
	uranusRing->draw();
	jupiterRing->draw();
	neptuneRing->draw();

	glUseProgram(*haloParticles);
	rt3d::setUniformMatrix4fv(*haloParticles, "projection", glm::value_ptr(projection));
	rt3d::setUniformMatrix4fv(*haloParticles, "modelview", glm::value_ptr(mvStack.top()));
	sunHalo->draw();


	//advanced particles
	glUseProgram(*flocking);
	rt3d::setUniformMatrix4fv(*flocking, "projection", glm::value_ptr(projection));
	rt3d::setUniformMatrix4fv(*flocking, "modelview", glm::value_ptr(mvStack.top()));
	//flockingParticles->draw();


	glUseProgram(*computeParticleShader);
	//mvStack.top() = camera->getLookAt();
	//rt3d::setUniformMatrix4fv(*computeParticleShader, "projection", glm::value_ptr(projection));
	rt3d::setUniformMatrix4fv(*computeParticleShader, "projection", glm::value_ptr(projection));
	rt3d::setUniformMatrix4fv(*computeParticleShader, "modelview", glm::value_ptr(mvStack.top()));//this probably is compute particle shader
	computeParticles->draw();

	glUseProgram(*computeShader);
	glm::vec3 BlackHolePosition = glm::vec3(5.0f,5.0f,0.0f);
	//rt3d::setUniform3fv(*computeShader, "BlackHolePos1", BlackHolePosition);
	//rt3d::setUniform(*computeShader, 0, BlackHolePosition);// sol->getStartPosition());//pass in location NOT SURE I WANT THIS!
	//const GLfloat gravity = 1000.0f;
	//rt3d::setUniform(*computeShader, 1, gravity);
	computeParticles->update();

	update();

	//draw gui last
	glUseProgram(guiShader);
	rt3d::setUniformMatrix4fv(guiShader, "projection", glm::value_ptr(projection));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	stringstream timeOutput;
	timeOutput << " Time Passed : " << timeNow << " days * 100 ";
	gui->textToTexture(timeOutput.str().c_str());
	gui->draw(-0.98f, 0.98f, guiShader);
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	mvStack.pop(); // initial matrix

	SDL_GL_SwapWindow(window); // swap buffers
}
bool StateNewton::handleSDLEvent(Game &context, SDL_Event *sdlEvent)
{
	//handles movement
	const Uint8 *keys = SDL_GetKeyboardState(NULL);

	if (keys[SDL_SCANCODE_W])
		cameraPosition = camera->moveForward(cameraPosition, cameraRotation, -1.0f);

	if (keys[SDL_SCANCODE_S])
		cameraPosition = camera->moveForward(cameraPosition, cameraRotation, 1.0f);

	if (keys[SDL_SCANCODE_A])
		cameraPosition = camera->moveRight(cameraPosition, cameraRotation, 1.0f);

	if (keys[SDL_SCANCODE_D])
		cameraPosition = camera->moveRight(cameraPosition, cameraRotation, -1.0f);

	if (keys[SDL_SCANCODE_COMMA])
		cameraPosition = camera->rotateCamera(cameraPosition, cameraRotation, -1.0f);

	if (keys[SDL_SCANCODE_PERIOD]) 
		cameraRotation -= 1.0f;

	if (keys[SDL_SCANCODE_Q])
		cameraPosition = camera->moveUp(cameraPosition);
	if (keys[SDL_SCANCODE_E])
		cameraPosition = camera->moveDown(cameraPosition);

	if (keys[SDL_SCANCODE_ESCAPE]) exit(0);

	return true;
}
void StateNewton::update()
{
	float m_time = 0;
	float timeStep = 0.01f;
	m_time += timeStep;
	sol->update(m_time);
	mercury->update(m_time);
	venus->update(m_time);
	earth->update(m_time);
	mars->update(m_time);
	jupiter->update(m_time);
	saturn->update(m_time);
	neptune->update(m_time);
	uranus->update(m_time);
	pluto->update(m_time);
	mercury->gravitationalForce(mercury, venus);
	mercury->perturb(mercury, venus, 1);
	venus->perturb(mercury, venus, 1);
	venus->perturb(venus, earth, 2);
	moon->update(m_time, earth);
	//venus->gravitationalForce(mercury, venus);
	//moon->update(m_time, earth);
	//venus->perturb(mercury, venus);
	m_time = 0;
	ring->update(saturn, -6.5f, -10.0f, 2.0f, 0.05f);
	ring1->update(saturn, -6.5f, -10.0f, 2.0f, 0.05f);
	ring2->update(saturn, -6.5f, -10.0f, 2.0f, 0.05f);
	uranusRing->updateVertical(uranus);
	jupiterRing->update(jupiter, -6.5f, -15.0f, 2.0f, 0.005f);
	neptuneRing->update(neptune, -3.0f, -10.0f, 2.0f, 0.005f);
	sunHalo->updateHalo(sol, -7.0f, -12.0f, 2.0f, 1.0f);

	//glUseProgram(*computeShader);
	//rt3d::setUniform(*computeShader, 0, glm::vec3(0.0f, 0.0f, 0.0f));// sol->getStartPosition());//pass in location NOT SURE I WANT THIS!
	//const GLfloat gravity = 1000.0f;
	//rt3d::setUniform(*computeShader, 1, gravity);
	//computeParticles->update();
}

