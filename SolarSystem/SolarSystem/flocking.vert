#version 440
//430
uniform mat4 modelview;
uniform mat4 projection;

int numOfParticles = 50;

out vec4 ex_Color;

layout(location = 0) in vec3 positions;
layout(location = 1) in vec3 velocities;
layout(location = 2) in vec3 separation;
layout(location = 3) in vec3 cohesion;
layout(location = 4) in vec3 alignment;

void calculateCohesion()
{
	for(int i = 0; i < numOfParticles; i+3)
	{
		
	
	}
}
void calculateAlignment()
{

}
void calculateSeparation()
{
}

float calculateDistance()
{
	float currentXA;
	float currentXB;
	float currentZA;
	float currentZB;
	float distX;
	float distZ;
	float distance;

	for (int i = 0; i < numOfParticles/2; i+=6)
	{
		currentXA = positions[i];
		currentZA = positions[i+2];
		currentXB = positions[i+3];
		currentZB = positions[i+5];

		float distX = currentXB - currentXA;
		float distZ = currentZB - currentZA;

		distance = sqrt(distX * distX) + (distZ * distZ);
	}
	return distance;
}

void moveAllBoids()
{
	
}
void main(void)
{

	float distance = calculateDistance();
	vec4 vertexPosition = modelview * vec4(positions, 1.0);
	ex_Color = vec4(1.0f,1.0f,1.0f,1.0f);
	gl_Position = (projection * vertexPosition);
}

