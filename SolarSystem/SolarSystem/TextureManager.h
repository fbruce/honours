#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include <GL/glew.h>
#include "LoadBitmap.h"

class TextureManager
{
private:
	GLuint textures[10];
	GLuint normals[10];
	GLuint i;
	LoadBitmap *loadBitmap;

public:
	TextureManager();
	void init();
	GLuint getTexture(int tex);
	GLuint getNormal(int tex);
};
#endif