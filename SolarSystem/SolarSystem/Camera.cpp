#include "Camera.h"

Camera::Camera()
{
	eye = glm::vec3(0.0f, 1.0f, 0.0f);
	at = glm::vec3(0.0f, 1.0f, -1.0f);
	up = glm::vec3(0.0f, 1.0f, 0.0f);
}
Camera::~Camera()
{
	delete this;
}
glm::vec3 Camera::moveForward(glm::vec3 pos, GLfloat angle, GLfloat d)
{
	return glm::vec3(pos.x + d*std::sin(r*DEG_TO_RADIAN), pos.y, pos.z - d*std::cos(r*DEG_TO_RADIAN));
}
glm::vec3 Camera::moveRight(glm::vec3 pos, GLfloat angle, GLfloat d)
{
	return glm::vec3(pos.x + d*std::cos(r*DEG_TO_RADIAN), pos.y, pos.z + d*std::sin(r*DEG_TO_RADIAN));
}
glm::vec3 Camera::moveUp(glm::vec3 pos)
{
	return glm::vec3(pos.x, pos.y + 0.1f, pos.z);
}
glm::vec3 Camera::moveDown(glm::vec3 pos)
{
	return glm::vec3(pos.x, pos.y - 0.1f, pos.z);
}
glm::vec3 Camera::rotateCamera(glm::vec3 pos, GLfloat angle, GLfloat d)
{
	return glm::vec3(pos.x, pos.y + d*std::sin(y*DEG_TO_RADIAN), pos.z);
}
