#ifndef COMPUTESHADERPARTICLES_H
#define COMPUTESHADERPARTICLES_H

#define DEG_TO_RADIAN 0.017453293

#include "GL/glew.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>


class ComputeShaderParticles
{
private:
	int numOfParticles;
	glm::vec4* positions;
	glm::vec4* velocities;
	GLuint vao[1];
	GLuint vbo[2];

public:
	ComputeShaderParticles(const int n);
	~ComputeShaderParticles();
	int getNumParticles(void) const { return numOfParticles; }
	glm::vec4 getPositions(void) const { return *positions; }
	glm::vec4 getVel(void) { return *velocities; }
	void init(void);
	void update(void);
	void draw(void);
};
#endif